#!/bin/sh -ex

ENVIRONMENT=$1
FORCE=$2

if [ -z "$ENVIRONMENT" ]
then
  echo "Usage: ./deploy.sh staging|production [--force]"
  exit 1
fi

if [ "$ENVIRONMENT" = "production" ]; then
  git fetch origin main:production
  git push origin production
fi

# Check if the '--force' argument is provided
EXTRA_VARS=""
if [ "$FORCE" = "--force" ]; then
  EXTRA_VARS="-e override_lock=true"
fi

pushd playbooks
source .venv/bin/activate
# Append $EXTRA_VARS to the ansible command if --force is provided
./ansible.sh -i environments/$ENVIRONMENT shared/deploy.yaml $EXTRA_VARS
deactivate
popd
