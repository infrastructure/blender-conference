Dear {% firstof user.profile.full_name user.email %},
{% block content %}{% endblock content %}

Manage your Conference profile: {{ profile_url }}
Find your Conference tickets here: {{ tickets_url }}

--
Kind regards,

Blender Conference Team
