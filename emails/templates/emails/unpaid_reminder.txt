{% extends "emails/base.txt" %}{% block content %}
You have placed a ticket order #{{ order.number }} for Blender Conference,
which will take place Thursday October 27 until Saturday October 29, 2022.

We haven't received your payment yet.
So we would like to know if you are coming and how you would like to
make your payment.

Maybe you already made your payment to our bank account, or paid by Paypal.
If so, please let us know.

If you have not paid yet, you may also pay cash upon arrival at the conference.
If you want to pay cash, please let us know.

Maybe you decided not to come. If that is the case, just let us
know by email, preferably within a week,
because the conference is almost full and there are people who really would like to come.

Please contact us by sending an email to {{ DEFAULT_REPLY_TO_EMAIL }} or simply replying to this email.

{% include "emails/components/ticket_order_info.txt" %}{% endblock content %}
