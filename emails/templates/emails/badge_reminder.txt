{% extends "emails/base.txt" %}{% block content %}
Blender Conference is around the corner!

The conference badges are going to be printed tomorrow, so this is your last chance to make sure your name and other details are looking good.

Check a preview of your badge at {{ site_url }}{{ ticket.get_absolute_url }} and do any last minute tweaks today!
{% endblock %}
