"""Implements Saleor webhook endpoints."""
from urllib.parse import urlparse
import hashlib
import hmac
import json
import logging

from django.conf import settings
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

import tickets.models
import tickets.tasks as tasks

logger = logging.getLogger(__name__)


class WebhookException(Exception):
    """Any exception that happens during webhook handling."""

    pass


def _expect_header(header_name, request) -> str:
    header_value = request.META.get(header_name)
    if not header_value:
        raise WebhookException(f'{header_name} is missing, {request.META}')
    return header_value


def _signature_for_payload(body: bytes, secret_key):
    if not secret_key:
        return ''
    hash = hmac.new(bytes(secret_key, 'utf-8'), body, hashlib.sha256)
    return hash.hexdigest()


@method_decorator(csrf_exempt, name='dispatch')
class OrderUpdated(View):
    """Handle ORDER_FULLY_PAID webhook event."""

    known_events = {'order_fully_paid'}

    def validate_request(self, request):
        """Check all expected headers and verify payload signature."""
        # Check content length
        header_content_length = _expect_header('CONTENT_LENGTH', request)
        expected_content_length = len(request.body)
        content_length = int(header_content_length)
        if expected_content_length != content_length:
            raise WebhookException('Unexpected content length')
        # Verify the domain
        domain = _expect_header('HTTP_SALEOR_DOMAIN', request)
        expected_domain = urlparse(settings.SALEOR_API_URL).netloc
        if domain != expected_domain:
            raise WebhookException('Unexpected domain')
        # Verify the signature
        signature = _expect_header('HTTP_SALEOR_SIGNATURE', request)
        secret = settings.SALEOR_WEBHOOK_SECRET
        if isinstance(secret, str):
            secret = secret.encode()
        signature_calculated = _signature_for_payload(request.body, settings.SALEOR_WEBHOOK_SECRET)
        if signature_calculated != signature:
            raise WebhookException('Signature mismatch')
        event = _expect_header('HTTP_SALEOR_EVENT', request)
        if event not in self.known_events:
            raise WebhookException('Unknown webhook event %s', event)

    def post(self, request):
        """Handle webhook payload."""
        try:
            self.validate_request(request)
            payload = json.loads(request.body)
            order_token = payload[0]['token']
            if tickets.models.Ticket.objects.filter(order_token=order_token).exists():
                tasks.handle_order_fully_paid(payload)
        except Exception:
            logger.exception('Handling of the webhook event failed, %s', request.META)
        return HttpResponse()
