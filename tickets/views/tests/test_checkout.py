from unittest.mock import patch

from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse_lazy

# from responses import _recorder
import responses

from tickets.tests.factories import UserFactory, SiteSettingsFactory, create_system_user
from tickets.tests.graphql import mock_saleor_responses, _clean_gq_calls
import tickets.models
import tickets.tasks as tasks


@override_settings(ACTIVE_PAYMENT_BACKEND='saleor')
class TestTicketBuyView(TestCase):
    maxDiff = None
    url = reverse_lazy('tickets:buy', kwargs={'sku': 'BC22'})

    def setUp(self):
        super().setUp()
        SiteSettingsFactory(site_id=1)
        responses.start()
        responses.assert_all_requests_are_fired = False
        mock_saleor_responses()

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    def test_get_displays_ticket_info_anonymous(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, '<h2>Blender Conference 2022: full ticket</h2>', html=True)
        self.assertContains(
            response,
            '<h1 id="product-price" class="m-0" data-amount="50.0" data-currency="EUR">€&nbsp;50.00</h1>',
            html=True,
        )
        self.assertContains(
            response,
            '<button type="submit" class="btn btn-block btn-primary">Buy Tickets</button>',
            html=True,
        )
        self.assertContains(
            response,
            '<input type="number" name="quantity" value="1" min="1" max="50" '
            'class="numberinput form-control" required id="id_quantity">',
            html=True,
        )
        self.assertContains(
            response,
            '<input type="hidden" name="variant_id" value="UHJvZHVjdFZhcmlhbnQ6OA==" id="id_variant_id">',
            html=True,
        )

    def test_get_displays_ticket_info(self):
        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, '<h2>Blender Conference 2022: full ticket</h2>', html=True)
        self.assertContains(
            response,
            '<h1 id="product-price" class="m-0" data-amount="50.0" data-currency="EUR">€&nbsp;50.00</h1>',
            html=True,
        )
        self.assertContains(
            response,
            '<button type="submit" class="btn btn-block btn-primary">Buy Tickets</button>',
            html=True,
        )
        self.assertContains(
            response,
            '<input type="number" name="quantity" value="1" min="1" max="50" '
            'class="numberinput form-control" required id="id_quantity">',
            html=True,
        )
        self.assertContains(
            response,
            '<input type="hidden" name="variant_id" value="UHJvZHVjdFZhcmlhbnQ6OA==" id="id_variant_id">',
            html=True,
        )

    # @_recorder.record(file_path='tickets/tests/saleor_validation_errors.yaml')
    def test_post_displays_validation_errors_anonymous(self):
        responses._add_from_file(file_path='tickets/tests/saleor_validation_errors.yaml')

        response = self.client.post(self.url, data={'variant_id': 'asdfasd', 'quantity': 'asdfasd'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'quantity': ['Enter a whole number.'],
                'variant_id': ['Ensure this value has at least 24 characters (it has 7).'],
            },
        )

    def test_post_displays_validation_errors(self):
        responses._add_from_file(file_path='tickets/tests/saleor_validation_errors.yaml')

        user = UserFactory()
        self.client.force_login(user)

        response = self.client.post(self.url, data={'variant_id': 'asdfasd', 'quantity': 'asdfasd'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'quantity': ['Enter a whole number.'],
                'variant_id': ['Ensure this value has at least 24 characters (it has 7).'],
            },
        )

    def test_post_creates_a_checkout(self):
        user = UserFactory(email='test.user@example.com')
        self.client.force_login(user)
        response = self.client.post(
            self.url, data={'variant_id': 'UHJvZHVjdFZhcmlhbnQ6OA==', 'quantity': 2}
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'], '/tickets/checkout/01e5ffc2-41e8-46ad-a4e0-b74eea517cef/'
        )
        mock_requests = _clean_gq_calls(responses)
        self.assertEqual(
            mock_requests[1].request.body,
            b'{"query": "mutation {\\n'
            b'  checkoutCreate(\\n'
            b'    input: {channel: \\"default-channel\\", '
            b'lines: [{quantity: 2, variantId: \\"UHJvZHVjdFZhcmlhbnQ6OA==\\"}],'
            b' validationRules: {'
            b'shippingAddress: {checkRequiredFields: false, checkFieldsFormat: true, enableFieldsNormalization: true},'
            b' billingAddress: {checkRequiredFields: false, checkFieldsFormat: true, enableFieldsNormalization: true}}'
            b'}\\n'
            b'  ) {\\n'
            b'    checkout {\\n'
            b'      id\\n'
            b'      token\\n'
            b'      totalPrice {\\n'
            b'        currency\\n'
            b'        gross {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'        net {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'        tax {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'      }\\n'
            b'      availablePaymentGateways {\\n'
            b'        id\\n'
            b'        name\\n'
            b'        config {\\n'
            b'          field\\n'
            b'          value\\n'
            b'        }\\n'
            b'      }\\n'
            b'    }\\n'
            b'    errors {\\n'
            b'      field\\n'
            b'      message\\n'
            b'      code\\n'
            b'      variants\\n'
            b'      lines\\n'
            b'      addressType\\n'
            b'    }\\n'
            b'  }\\n'
            b'}"}',
        )

    def test_post_creates_a_checkout_anonymous(self):
        response = self.client.post(
            self.url, data={'variant_id': 'UHJvZHVjdFZhcmlhbnQ6OA==', 'quantity': 2}
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'], '/tickets/checkout/01e5ffc2-41e8-46ad-a4e0-b74eea517cef/'
        )
        mock_requests = _clean_gq_calls(responses)
        self.assertEqual(
            mock_requests[1].request.body,
            b'{"query": "mutation {\\n'
            b'  checkoutCreate(\\n'
            b'    input: {channel: \\"default-channel\\", '
            b'lines: [{quantity: 2, variantId: \\"UHJvZHVjdFZhcmlhbnQ6OA==\\"}],'
            b' validationRules: {'
            b'shippingAddress: {checkRequiredFields: false, checkFieldsFormat: true, enableFieldsNormalization: true},'
            b' billingAddress: {checkRequiredFields: false, checkFieldsFormat: true, enableFieldsNormalization: true}}}'
            b'\\n'
            b'  ) {\\n'
            b'    checkout {\\n'
            b'      id\\n'
            b'      token\\n'
            b'      totalPrice {\\n'
            b'        currency\\n'
            b'        gross {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'        net {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'        tax {\\n'
            b'          amount\\n'
            b'          currency\\n'
            b'        }\\n'
            b'      }\\n'
            b'      availablePaymentGateways {\\n'
            b'        id\\n'
            b'        name\\n'
            b'        config {\\n'
            b'          field\\n'
            b'          value\\n'
            b'        }\\n'
            b'      }\\n'
            b'    }\\n'
            b'    errors {\\n'
            b'      field\\n'
            b'      message\\n'
            b'      code\\n'
            b'      variants\\n'
            b'      lines\\n'
            b'      addressType\\n'
            b'    }\\n'
            b'  }\\n'
            b'}"}',
        )


@override_settings(SYSTEM_USER_ID=1, ACTIVE_PAYMENT_BACKEND='saleor')
class TestCheckoutView(TestCase):
    maxDiff = None
    url = reverse_lazy(
        'tickets:checkout', kwargs={'checkout_token': '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'}
    )

    def setUp(self):
        super().setUp()
        create_system_user()
        SiteSettingsFactory(site_id=1)

    def test_get_requires_login(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={self.url}')

    def test_post_requires_login(self):
        response = self.client.post(self.url, {})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={self.url}')

    def test_get_redirects_on_invalid_checkout_token(self):
        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/')

    def test_post_redirects_on_invalid_checkout_token(self):
        user = UserFactory()
        self.client.force_login(user)
        response = self.client.post(self.url, {})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/')

    @responses.activate
    def test_post_displays_validation_errors(self):
        mock_saleor_responses()
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()
        response = self.client.post(self.url, {})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {
                'amount': ['This field is required.'],
                'gateway': ['This field is required.'],
                'payment_method_nonce': ['This field is required.'],
            },
        )

    @responses.activate
    def test_post_selecting_bank_should_set_nonce_to_pending(self):
        self.assertEqual(tickets.models.Ticket.objects.count(), 0)
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_saleor_responses()
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()

        response = self.client.post(
            self.url,
            {
                'amount': 100.00,
                'gateway': 'bank',
                'payment_method_nonce': 'some-previous-nonce-that-must-be-ignored',
                'device_data': '',
            },
        )

        mock_requests = _clean_gq_calls(responses)
        # Check that a payment has been created with a "pending" token
        self.assertTrue(
            b'"mutation {\\n  checkoutPaymentCreate(\\n    token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\"'
            b'\\n    input: {gateway: \\"mirumee.payments.dummy\\", token: \\"pending\\"'
            in mock_requests[2].request.body
        )
        # Check the successful checkout flow redirects to the ticket details
        ticket = tickets.models.Ticket.objects.first()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/account/ticket/{ticket.token}/')

    @responses.activate
    @patch(
        'tickets.tasks.send_mail_bank_transfer_required',
        new=tasks.send_mail_bank_transfer_required.task_function,
    )
    def test_post_multiple_unpaid_tickets(self):
        self.assertEqual(tickets.models.Ticket.objects.count(), 0)
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_saleor_responses(checkout_data={'email': None})
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()

        response = self.client.post(
            self.url,
            {
                'amount': 100.00,
                'gateway': 'bank',
                'payment_method_nonce': 'bank',
                'device_data': '',
            },
        )

        # Check that a ticket was created
        self.assertEqual(tickets.models.Ticket.objects.count(), 1)
        # No ticket claims were created because more than 1 ticket was bought
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_requests = _clean_gq_calls(responses)
        self.assertEqual(len(mock_requests), 5)
        self.assertTrue(
            b'checkout(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[0].request.body
        )
        self.assertTrue(
            b'mutation {\\n  updateMetadata(\\n    id: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\"'
            in mock_requests[1].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutPaymentCreate(\\n    token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\"'
            in mock_requests[2].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutComplete(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[3].request.body
        )
        self.assertTrue(
            b'{"query": "{\\n  orderByToken(token: \\"8ef27213-845a-4bb9-a917-d342b7566a1c\\")'
            in mock_requests[4].request.body
        )
        ticket = tickets.models.Ticket.objects.first()
        self.assertEqual(str(ticket.order_token), '8ef27213-845a-4bb9-a917-d342b7566a1c')
        self.assertEqual(ticket.order_number, '37')
        self.assertEqual(
            ticket.order_id, 'T3JkZXI6OGVmMjcyMTMtODQ1YS00YmI5LWE5MTctZDM0MmI3NTY2YTFj'
        )
        self.assertEqual(ticket.quantity, 2)
        self.assertEqual(ticket.sku, 'BC22')
        self.assertEqual(ticket.user, user)
        self.assertFalse(ticket.is_paid)
        # Check the successful checkout flow redirects to the ticket details
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/account/ticket/{ticket.token}/')

    @responses.activate
    def test_get_updates_email(self):
        self.assertEqual(tickets.models.Ticket.objects.count(), 0)
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_saleor_responses(checkout_data={'email': None})
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()

        self.client.get(self.url)

        mock_requests = _clean_gq_calls(responses)
        # Check that a payment has been created with a "pending" token
        self.assertTrue(
            b'mutation {\\n  checkoutEmailUpdate(\\n    '
            b'email: \\"' + user.email.encode() + b'\\"\\n    '
            b'id: \\"Q2hlY2tvdXQ6Mzc1ZGI4NjctYzM1Zi00NTk2LWEzYzMtMzNiNDNiZjEzNTli\\"\\n  )'
            in mock_requests[1].request.body
        )

    @responses.activate
    @patch(
        'tickets.tasks.send_mail_bank_transfer_required',
        new=tasks.send_mail_bank_transfer_required.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    @patch(
        'tickets.tasks.generate_invoice',
        new=tasks.generate_invoice.task_function,
    )
    @patch(
        'tickets.tasks.update_profile_from_ticket_order',
        new=tasks.update_profile_from_ticket_order.task_function,
    )
    def test_post_single_paid_ticket_confirmation_and_invoice_mail_sent(self):
        self.assertEqual(tickets.models.Ticket.objects.count(), 0)
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_saleor_responses(quantity=1, is_paid=True)
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()

        self.client.post(
            self.url,
            {
                'amount': 50.00,
                'gateway': 'braintree',
                'payment_method_nonce': 'fake-braintree-nonce',
                'device_data': '{"correlation_id":"foobar-deadbeef"}',
            },
        )

        # Check that a ticket was created
        self.assertEqual(tickets.models.Ticket.objects.count(), 1)
        ticket = tickets.models.Ticket.objects.first()
        # Ticket claim was created immediately because only 1 ticket was bought
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 1)
        mock_requests = _clean_gq_calls(responses)
        # Check that a payment has been created with a "pending" token
        self.assertEqual(len(mock_requests), 6)
        claim = tickets.models.TicketClaim.objects.first()
        self.assertIsNotNone(claim.confirmation_sent_at)
        # Check that invoice was generated and send out immediately
        self.assertTrue(
            b'checkout(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[0].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutPaymentCreate(\\n    token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\"'
            in mock_requests[1].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutComplete(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[2].request.body
        )
        self.assertTrue(
            b'invoiceRequest' in mock_requests[3].request.body, mock_requests[3].request.body
        )
        self.assertTrue(
            b'{"query": "{\\n  orderByToken(token: \\"8ef27213-845a-4bb9-a917-d342b7566a1c\\")'
            in mock_requests[4].request.body,
            mock_requests[4].request.body,
        )
        self.assertEqual(
            ticket.invoice_url,
            'http://staging.shop.blender.org/media/invoices/'
            'invoice-12062022-order-6df3e673-046f-47e3-9df1-'
            '3a03968d3f60-1b4fff39-9a2f-4379-_yIDif3H.pdf',
        )
        # Check that confirmation email was sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket confirmed'
        )
        # Check that profile info is updated based on billing address
        user.refresh_from_db()
        self.assertEqual(user.profile.full_name, 'Jane Doe')
        self.assertEqual(user.profile.company, 'Good Company Inc.')
        self.assertEqual(user.profile.country.name, 'Netherlands')

    @responses.activate
    @patch(
        'tickets.tasks.send_mail_bank_transfer_required',
        new=tasks.send_mail_bank_transfer_required.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    @patch(
        'tickets.tasks.generate_invoice',
        new=tasks.generate_invoice.task_function,
    )
    def test_post_multiple_paid_tickets(self):
        self.assertEqual(tickets.models.Ticket.objects.count(), 0)
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_saleor_responses(is_paid=True)
        user = UserFactory()
        self.client.force_login(user)
        session = self.client.session
        session['checkout_token'] = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        session.save()

        response = self.client.post(
            self.url,
            {
                'amount': 100.00,
                'gateway': 'braintree',
                'payment_method_nonce': 'fake-nonce',
                'device_data': '',
            },
        )

        # Check that a ticket was created
        self.assertEqual(tickets.models.Ticket.objects.count(), 1)
        # No ticket claims were created because more than 1 ticket was bought
        self.assertEqual(tickets.models.TicketClaim.objects.count(), 0)
        mock_requests = _clean_gq_calls(responses)
        # Check that a payment has been created with a "pending" token
        self.assertEqual(len(mock_requests), 5)
        self.assertTrue(
            b'checkout(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[0].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutPaymentCreate(\\n    token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\"'
            in mock_requests[1].request.body
        )
        self.assertTrue(
            b'"mutation {\\n  checkoutComplete(token: \\"01e5ffc2-41e8-46ad-a4e0-b74eea517cef\\")'
            in mock_requests[2].request.body
        )
        self.assertTrue(
            b'invoiceRequest' in mock_requests[3].request.body, mock_requests[3].request.body
        )
        self.assertTrue(
            b'{"query": "{\\n  orderByToken(token: \\"8ef27213-845a-4bb9-a917-d342b7566a1c\\")'
            in mock_requests[4].request.body,
            mock_requests[4].request.body,
        )
        ticket = tickets.models.Ticket.objects.first()
        self.assertEqual(str(ticket.order_token), '8ef27213-845a-4bb9-a917-d342b7566a1c')
        self.assertEqual(ticket.order_number, '37')
        self.assertEqual(
            ticket.order_id, 'T3JkZXI6OGVmMjcyMTMtODQ1YS00YmI5LWE5MTctZDM0MmI3NTY2YTFj'
        )
        self.assertEqual(ticket.quantity, 2)
        self.assertEqual(ticket.sku, 'BC22')
        self.assertEqual(ticket.user, user)
        self.assertTrue(ticket.is_paid)
        # Check the successful checkout flow redirects to the ticket details
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/account/ticket/{ticket.token}/')
        # Check that "tickets paid" confirmation email was sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Tickets payment confirmed'
        )
        self.assertIn(ticket.claim_url, email.body)


@override_settings(
    SYSTEM_USER_ID=1, STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage'
)
class TestProductsTableView(TestCase):
    url = reverse_lazy('tickets:products-table')

    def setUp(self):
        super().setUp()
        create_system_user()
        SiteSettingsFactory(site_id=1)

    # Anonymous, no tickets sold
    def test_anonymous_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
