from unittest.mock import patch

from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse
import responses

# from responses import _recorder

from tickets.tests.factories import (
    UserFactory,
    SiteSettingsFactory,
    TicketFactory,
)
from tickets.tests.graphql import mock_saleor_responses
import tickets.tasks as tasks


@override_settings(ACTIVE_PAYMENT_BACKEND='saleor')
class TestTicketPage(TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        SiteSettingsFactory(site_id=1)

    def test_get_requires_login(self):
        ticket = TicketFactory(quantity=2)

        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={ticket.get_absolute_url()}')

    @responses.activate
    def test_get_does_not_display_claim_info_for_single_paid_ticket(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=1,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=True,
        )
        ticket.process_new_ticket()

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.claim_url, '')
        self.assertNotContains(response, 'Unclaimed tickets remaining')
        self.assertNotContains(response, 'can be claimed using the following link')
        self.assertNotContains(response, f'/tickets/claim/{ticket.token}/')

    @responses.activate
    def test_get_displays_claim_info_for_multiple_tickets(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=False
        )

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Unclaimed tickets remaining: <strong>2</strong>')
        self.assertContains(response, 'Tickets can be claimed using the following link')
        self.assertContains(response, 'Make sure all tickets are claimed')
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')
        self.assertContains(response, ticket.claim_url)
        # Check that a "badge preview" is not shown
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertNotContains(response, 'Your Badge')

    @responses.activate
    def test_get_displays_claim_info_for_single_ticket(self):
        mock_saleor_responses()
        another_ticket = TicketFactory(is_paid=True)
        another_ticket.process_new_ticket()
        self.assertTrue(another_ticket.is_claimed_by(another_ticket.user.pk))
        ticket = TicketFactory(
            quantity=1,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=False,
            edition=another_ticket.edition,
            user=another_ticket.user,
        )
        self.assertFalse(ticket.is_claimed_by(another_ticket.user.pk))

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')
        self.assertContains(response, 'Ticket can be claimed using the following link')
        self.assertContains(response, 'Make sure this ticket is claimed')
        self.assertContains(response, ticket.claim_url)
        # Check that a "badge preview" is not shown
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertNotContains(response, 'Your Badge')

    @responses.activate
    def test_get_displays_badge_preview_to_whomever_claimed(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=3,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=True,
        )
        # This account has bought the ticket, but didn't claim it
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        # These two accounts has claimed 2 out of 3 tickets
        (who_claimed_1, who_claimed_2) = (UserFactory(), UserFactory())
        who_claimed_1.profile.full_name = 'Жанна До'
        who_claimed_1.profile.company = 'Жанна Inc.'
        who_claimed_1.profile.country = 'US'
        who_claimed_1.profile.save()
        who_claimed_2.profile.full_name = 'John Smith'
        who_claimed_2.profile.save()
        ticket.attendees.add(who_claimed_1)
        ticket.attendees.add(who_claimed_2)

        self.client.force_login(who_claimed_1)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        # Check that claim warning IS NOT shows to the accounts which claimed the tickets
        self.assertNotContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertNotContains(response, 'Ticket can be claimed using the following link')
        self.assertNotContains(response, 'Make sure this ticket is claimed')
        self.assertNotContains(response, ticket.claim_url)
        # Check that a "badge preview" is shown
        self.assertTrue(ticket.is_claimed_by(who_claimed_1.pk))
        self.assertContains(response, 'Your Badge')
        self.assertContains(response, 'Жанна До')
        self.assertContains(response, 'Жанна Inc.')
        self.assertContains(response, 'United States')

        self.client.force_login(who_claimed_2)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        # Check that claim warning IS NOT shows to the accounts which claimed the tickets
        self.assertNotContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertNotContains(response, 'Ticket can be claimed using the following link')
        self.assertNotContains(response, 'Make sure this ticket is claimed')
        self.assertNotContains(response, ticket.claim_url)
        # Check that a "badge preview" is shown
        self.assertTrue(ticket.is_claimed_by(who_claimed_2.pk))
        self.assertContains(response, 'Your Badge')
        self.assertContains(response, 'John Smith')


@override_settings(ACTIVE_PAYMENT_BACKEND='saleor')
class TestTicketClaimView(TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        SiteSettingsFactory(site_id=1)

    def test_claim_url_empty_when_paid_and_quantity_one(self):
        ticket = TicketFactory(quantity=1, is_paid=True)
        ticket.process_new_ticket()
        self.assertEqual(ticket.claim_url, '')

    def test_claim_url_not_empty_when_quantity_more_than_one(self):
        ticket = TicketFactory(quantity=5)
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')

    def test_get_requires_login(self):
        ticket = TicketFactory(quantity=2)
        url = ticket.claim_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={url}')

    def test_post_requires_login(self):
        ticket = TicketFactory(quantity=2)
        url = ticket.claim_url
        response = self.client.post(url, {})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], f'/oauth/login?next={url}')

    @responses.activate
    def test_post_validation_errors(self):
        mock_saleor_responses()
        ticket = TicketFactory(quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c')
        url = ticket.claim_url

        self.client.force_login(ticket.user)
        response = self.client.post(url, {})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {'full_name': ['This field is required.']},
        )

    @responses.activate
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_general_info',
        new=tasks.send_mail_general_info.task_function,
    )
    def test_post_creates_a_ticket_claim_and_updates_profile(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=True
        )
        url = ticket.claim_url

        attendee = UserFactory()
        self.assertFalse(ticket.is_claimed_by(attendee.pk))
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertEqual(ticket.unclaimed, 2)

        self.client.force_login(attendee)
        response = self.client.post(
            url,
            {
                'full_name': "Жанна Д'Арк",
                'company': 'Good Company Inc.',
                'country': 'FR',
                'title': 'Revolutionary generalist',
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertTrue(ticket.is_claimed_by(attendee.pk))
        self.assertEqual(ticket.unclaimed, 1)
        self.assertEqual(
            response['Location'], reverse('tickets:detail', kwargs={'ticket_token': ticket.token})
        )
        attendee.profile.refresh_from_db()
        self.assertEqual(attendee.profile.full_name, "Жанна Д'Арк")
        self.assertEqual(attendee.profile.company, 'Good Company Inc.')
        self.assertEqual(attendee.profile.title, 'Revolutionary generalist')
        self.assertEqual(attendee.profile.country.name, 'France')

        self.assertEqual(len(mail.outbox), 1)  # TODO should be 2 when general info is sent

        # Check that ticket confirmation email was sent
        email = mail.outbox[0]
        self.assertEqual(email.to, [attendee.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket confirmed'
        )
        self.assertNotIn(ticket.claim_url, email.body)
        self.assertIn(ticket.get_absolute_url(), email.body)

        return  # TODO general info isn't yet sent automatically
        # Check that general info email was sent
        email = mail.outbox[1]
        self.assertEqual(email.to, [attendee.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: General Information'
        )
        self.assertNotIn(ticket.claim_url, email.body)
        self.assertIn(ticket.get_absolute_url(), email.body)

    @responses.activate
    def test_post_cannot_claim_multiple_tickets(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=True
        )
        attendee = UserFactory()
        ticket.attendees.add(attendee)
        self.assertTrue(ticket.is_claimed_by(attendee.pk))
        self.assertEqual(ticket.unclaimed, 1)

        url = ticket.claim_url
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 1)
        self.assertEqual(
            response.context['form'].errors,
            {
                '__all__': [
                    f'Your already have a ticket for {ticket.edition}. '
                    'View your tickets in <a href="/account/tickets/">My Tickets</a>.',
                ]
            },
        )

    @responses.activate
    def test_post_cannot_claim_more_than_available_quantity(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=True
        )
        attendee1 = UserFactory()
        attendee2 = UserFactory()
        ticket.attendees.add(attendee1, attendee2)
        self.assertTrue(ticket.is_claimed_by(attendee1.pk))
        self.assertTrue(ticket.is_claimed_by(attendee2.pk))
        self.assertEqual(ticket.unclaimed, 0)

        url = reverse('tickets:claim', kwargs={'ticket_token': ticket.token})
        attendee = UserFactory()
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 0)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['All available tickets had already been claimed.']},
        )

    @responses.activate
    def test_post_cannot_claim_unpaid_ticket(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=False
        )

        attendee = UserFactory()
        self.assertFalse(ticket.is_claimed_by(attendee.pk))
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertEqual(ticket.unclaimed, 2)

        url = ticket.claim_url
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 2)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['Ticket cannot be claimed: payment pending.']},
        )


@override_settings(ACTIVE_PAYMENT_BACKEND='stripe')
class TestStripeTicketPage(TestCase):
    maxDiff = None

    def setUp(self):
        responses.start()
        # Load previously recorded Stripe responses from YAML files
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml'
        )

        SiteSettingsFactory(site_id=1)
        super().setUp()

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    def test_get_does_not_display_claim_info_for_single_paid_ticket(self):
        ticket = TicketFactory(
            is_paid=True,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            quantity=1,
            sku='BCON22 One Day',
        )
        ticket.process_new_ticket()

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.claim_url, '')
        self.assertNotContains(response, 'Unclaimed tickets remaining')
        self.assertNotContains(response, 'can be claimed using the following link')
        self.assertNotContains(response, f'/tickets/claim/{ticket.token}/')

    def test_get_displays_claim_info_for_multiple_tickets(self):
        ticket = TicketFactory(
            is_paid=False,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            quantity=2,
            sku='BCON22 One Day',
        )

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Unclaimed tickets remaining: <strong>2</strong>')
        self.assertContains(response, 'Tickets can be claimed using the following link')
        self.assertContains(response, 'Make sure all tickets are claimed')
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')
        self.assertContains(response, ticket.claim_url)
        # Check that a "badge preview" is not shown
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertNotContains(response, 'Your Badge')

    # @_recorder.record(file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml')
    def test_get_displays_claim_info_for_single_ticket(self):
        another_ticket = TicketFactory(is_paid=True)
        another_ticket.process_new_ticket()
        self.assertTrue(another_ticket.is_claimed_by(another_ticket.user.pk))
        ticket = TicketFactory(
            edition=another_ticket.edition,
            is_paid=False,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            quantity=1,
            sku='BCON22 One Day',
            user=another_ticket.user,
        )
        self.assertFalse(ticket.is_claimed_by(another_ticket.user.pk))

        self.client.force_login(ticket.user)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')
        self.assertContains(response, 'Ticket can be claimed using the following link')
        self.assertContains(response, 'Make sure this ticket is claimed')
        self.assertContains(response, ticket.claim_url)
        # Check that a "badge preview" is not shown
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertNotContains(response, 'Your Badge')

    def test_get_displays_badge_preview_to_whomever_claimed(self):
        ticket = TicketFactory(
            quantity=3,
            is_paid=True,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
        )
        # This account has bought the ticket, but didn't claim it
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        # These two accounts has claimed 2 out of 3 tickets
        (who_claimed_1, who_claimed_2) = (UserFactory(), UserFactory())
        who_claimed_1.profile.full_name = 'Жанна До'
        who_claimed_1.profile.company = 'Жанна Inc.'
        who_claimed_1.profile.country = 'US'
        who_claimed_1.profile.save()
        who_claimed_2.profile.full_name = 'John Smith'
        who_claimed_2.profile.save()
        ticket.attendees.add(who_claimed_1)
        ticket.attendees.add(who_claimed_2)

        self.client.force_login(who_claimed_1)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        # Check that claim warning IS NOT shows to the accounts which claimed the tickets
        self.assertNotContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertNotContains(response, 'Ticket can be claimed using the following link')
        self.assertNotContains(response, 'Make sure this ticket is claimed')
        self.assertNotContains(response, ticket.claim_url)
        # Check that a "badge preview" is shown
        self.assertTrue(ticket.is_claimed_by(who_claimed_1.pk))
        self.assertContains(response, 'Your Badge')
        self.assertContains(response, 'Жанна До')
        self.assertContains(response, 'Жанна Inc.')
        self.assertContains(response, 'United States')

        self.client.force_login(who_claimed_2)
        response = self.client.get(ticket.get_absolute_url())

        self.assertEqual(response.status_code, 200)
        # Check that claim warning IS NOT shows to the accounts which claimed the tickets
        self.assertNotContains(response, 'Unclaimed tickets remaining: <strong>1</strong>')
        self.assertNotContains(response, 'Ticket can be claimed using the following link')
        self.assertNotContains(response, 'Make sure this ticket is claimed')
        self.assertNotContains(response, ticket.claim_url)
        # Check that a "badge preview" is shown
        self.assertTrue(ticket.is_claimed_by(who_claimed_2.pk))
        self.assertContains(response, 'Your Badge')
        self.assertContains(response, 'John Smith')


@override_settings(ACTIVE_PAYMENT_BACKEND='stripe')
class TestStripeTicketClaimView(TestCase):
    maxDiff = None

    def setUp(self):
        responses.start()
        # Load previously recorded Stripe responses from YAML files
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml'
        )

        SiteSettingsFactory(site_id=1)
        super().setUp()

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    def test_post_validation_errors(self):
        ticket = TicketFactory(
            quantity=2,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
        )
        url = ticket.claim_url

        self.client.force_login(ticket.user)
        response = self.client.post(url, {})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['form'].errors,
            {'full_name': ['This field is required.']},
        )

    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_general_info',
        new=tasks.send_mail_general_info.task_function,
    )
    def test_post_creates_a_ticket_claim_and_updates_profile(self):
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
            quantity=2,
            is_paid=True,
        )
        url = ticket.claim_url

        attendee = UserFactory()
        self.assertFalse(ticket.is_claimed_by(attendee.pk))
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertEqual(ticket.unclaimed, 2)

        self.client.force_login(attendee)
        response = self.client.post(
            url,
            {
                'full_name': "Жанна Д'Арк",
                'company': 'Good Company Inc.',
                'country': 'FR',
                'title': 'Revolutionary generalist',
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertTrue(ticket.is_claimed_by(attendee.pk))
        self.assertEqual(ticket.unclaimed, 1)
        self.assertEqual(
            response['Location'], reverse('tickets:detail', kwargs={'ticket_token': ticket.token})
        )
        attendee.profile.refresh_from_db()
        self.assertEqual(attendee.profile.full_name, "Жанна Д'Арк")
        self.assertEqual(attendee.profile.company, 'Good Company Inc.')
        self.assertEqual(attendee.profile.title, 'Revolutionary generalist')
        self.assertEqual(attendee.profile.country.name, 'France')

        self.assertEqual(len(mail.outbox), 1)  # TODO should be 2 when general info is sent

        # Check that ticket confirmation email was sent
        email = mail.outbox[0]
        self.assertEqual(email.to, [attendee.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket confirmed'
        )
        self.assertNotIn(ticket.claim_url, email.body)
        self.assertIn(ticket.get_absolute_url(), email.body)

        return  # TODO general info isn't yet sent automatically
        # Check that general info email was sent
        email = mail.outbox[1]
        self.assertEqual(email.to, [attendee.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: General Information'
        )
        self.assertNotIn(ticket.claim_url, email.body)
        self.assertIn(ticket.get_absolute_url(), email.body)

    def test_post_cannot_claim_multiple_tickets(self):
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
            quantity=2,
            is_paid=True,
        )
        attendee = UserFactory()
        ticket.attendees.add(attendee)
        self.assertTrue(ticket.is_claimed_by(attendee.pk))
        self.assertEqual(ticket.unclaimed, 1)

        url = ticket.claim_url
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 1)
        self.assertEqual(
            response.context['form'].errors,
            {
                '__all__': [
                    f'Your already have a ticket for {ticket.edition}. '
                    'View your tickets in <a href="/account/tickets/">My Tickets</a>.',
                ]
            },
        )

    def test_post_cannot_claim_more_than_available_quantity(self):
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
            quantity=2,
            is_paid=True,
        )
        attendee1 = UserFactory()
        attendee2 = UserFactory()
        ticket.attendees.add(attendee1, attendee2)
        self.assertTrue(ticket.is_claimed_by(attendee1.pk))
        self.assertTrue(ticket.is_claimed_by(attendee2.pk))
        self.assertEqual(ticket.unclaimed, 0)

        url = reverse('tickets:claim', kwargs={'ticket_token': ticket.token})
        attendee = UserFactory()
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 0)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['All available tickets had already been claimed.']},
        )

    def test_post_cannot_claim_unpaid_ticket(self):
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            sku='BCON22 One Day',
            quantity=2,
            is_paid=False,
        )

        attendee = UserFactory()
        self.assertFalse(ticket.is_claimed_by(attendee.pk))
        self.assertFalse(ticket.is_claimed_by(ticket.user.pk))
        self.assertEqual(ticket.unclaimed, 2)

        url = ticket.claim_url
        self.client.force_login(attendee)
        response = self.client.post(url, {'full_name': "Жанна Д'Арк"})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.unclaimed, 2)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['Ticket cannot be claimed: payment pending.']},
        )
