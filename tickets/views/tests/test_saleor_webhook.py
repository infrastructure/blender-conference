from unittest.mock import patch

from django.test import TestCase, override_settings
from django.urls import reverse
import responses

from tickets.tests.factories import SiteSettingsFactory, TicketFactory, create_system_user
from tickets.tests.graphql import mock_saleor_responses, _read_graphql_fixture, _clean_gq_calls
import tickets.tasks as tasks


@override_settings(
    SALEOR_WEBHOOK_SECRET='GDFasdWEFAsdAwfs!#$@ASadaVCXX',
    SYSTEM_USER_ID=1,
    ACTIVE_PAYMENT_BACKEND='saleor',
)
class TestWebhookOrderFullyPaid(TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        SiteSettingsFactory(site_id=1)
        create_system_user()

    @responses.activate
    @patch(
        'tickets.tasks.generate_invoice',
        new=tasks.generate_invoice.task_function,
    )
    @patch(
        'tickets.tasks.handle_order_fully_paid',
        new=tasks.handle_order_fully_paid.task_function,
    )
    def test_order_full_paid(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=2,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=False,
            order_id='T3JkZXI6OGVmMjcyMTMtODQ1YS00YmI5LWE5MTctZDM0MmI3NTY2YTFj',
        )
        self.assertFalse(ticket.is_paid)
        self.assertEqual(ticket.unclaimed, 2)

        url = reverse('tickets:webhook-saleor')
        payload = _read_graphql_fixture('webhookOrderFullyPaid')
        headers = {
            'HTTP_SALEOR_EVENT': 'order_fully_paid',
            'HTTP_SALEOR_DOMAIN': 'staging.shop.blender.org',
            'HTTP_SALEOR_SIGNATURE': 'e39611d7390274305f4810060a94cdf85e420b1e2afeb6d5dfa1c293fd620244',  # noqa: E501
        }
        response = self.client.post(url, payload, content_type='application/json', **headers)

        self.assertEqual(response.status_code, 200)
        ticket.refresh_from_db()
        self.assertTrue(ticket.is_paid)
        # Check that invoice was generated
        mock_requests = _clean_gq_calls(responses)
        self.assertEqual(len(mock_requests), 1)
        self.assertTrue(
            b'invoiceRequest' in mock_requests[0].request.body, responses.calls[0].request.body
        )
        self.assertEqual(
            ticket.invoice_url,
            'http://staging.shop.blender.org/media/invoices/'
            'invoice-12062022-order-6df3e673-046f-47e3-9df1-3a03968d3f60-1b4fff39-9a2f-4379-_yIDif3H.pdf',
        )
        self.assertEqual(ticket.unclaimed, 2)

    @responses.activate
    @patch(
        'tickets.tasks.generate_invoice',
        new=tasks.generate_invoice.task_function,
    )
    @patch(
        'tickets.tasks.handle_order_fully_paid',
        new=tasks.handle_order_fully_paid.task_function,
    )
    def test_order_full_paid_single_ticket_gets_automatically_claimed(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=1,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=False,
            order_id='T3JkZXI6OGVmMjcyMTMtODQ1YS00YmI5LWE5MTctZDM0MmI3NTY2YTFj',
        )
        self.assertFalse(ticket.is_paid)
        self.assertEqual(ticket.unclaimed, 1)

        url = reverse('tickets:webhook-saleor')
        payload = _read_graphql_fixture('webhookOrderFullyPaid')
        headers = {
            'HTTP_SALEOR_EVENT': 'order_fully_paid',
            'HTTP_SALEOR_DOMAIN': 'staging.shop.blender.org',
            'HTTP_SALEOR_SIGNATURE': 'e39611d7390274305f4810060a94cdf85e420b1e2afeb6d5dfa1c293fd620244',  # noqa: E501
        }
        response = self.client.post(url, payload, content_type='application/json', **headers)

        self.assertEqual(response.status_code, 200)
        ticket.refresh_from_db()
        self.assertTrue(ticket.is_paid)
        # Check that invoice was generated
        mock_requests = _clean_gq_calls(responses)
        self.assertEqual(len(mock_requests), 1)
        # Check that ticket got claimed automatically
        self.assertEqual(ticket.unclaimed, 0)
