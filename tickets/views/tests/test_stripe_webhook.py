from io import BytesIO
from unittest.mock import patch
import json

from freezegun import freeze_time

from PyPDF2 import PdfReader
from background_task.models import Task
from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse

# from responses import _recorder
import responses

from tickets.tests.factories import (
    ProductFactory,
    SiteSettingsFactory,
    TicketFactory,
    UserFactory,
    create_system_user,
)
from tickets.models import Ticket
import tickets.tasks

expected_invoice_text_with_vat = '''Invoice number:
ST-1
Date issued:
2024-05-27
Date paid:
2024-05-23
Payment method:
RABONL2U**********5264 via iDEALBilling Address:
jane.doe@example.org
Jane Doe
Some Street 12
Amsterdam 1111AB
Netherlands
Products Price Quantity Total Price
BCON24 Full Ticket
Incl. VAT 21% (Entry)
Incl. VAT 9% (Food)€ 365.00
€ 31.67
€ 15.077 € 2555.00
VAT (21%) € 221.69
VAT (9%) € 105.49
Subtotal (incl. VAT) € 2555.00
Total € 2555.00 Invoice
Blender Institute B.V.
Buikslotermeerplein 161
1025 ET Amsterdam, The Netherlands
VATIN: NL818152011B01'''  # noqa: W291

expected_invoice_text = '''Invoice number:
ST-1234
Date issued:
2024-05-27
Date paid:
2024-05-23
Payment method:
RABONL2U**********5264 via iDEALBilling Address:
jane.doe@example.org
Jane Doe
Some Street 12
Amsterdam 1111AB
Netherlands
Products Price Quantity Total Price
BCON24 Full Ticket € 365.00 7 € 2555.00
Subtotal (incl. VAT) € 2555.00
Total € 2555.00 Invoice
Blender Institute B.V.
Buikslotermeerplein 161
1025 ET Amsterdam, The Netherlands
VATIN: NL818152011B01'''  # noqa: W291

expected_invoice_text_with_bank_transfer_instructions = '''Invoice number:
{order_number}
Date issued:
{created}Billing Address:
{email}
{name}
Some Street 12
{city} {postal_code}
Netherlands
Products Price Quantity Total Price
BCON24 Full Ticket € 365.00 7 € 2555.00
Subtotal (incl. VAT) € 2555.00
Total € 2555.00 
Transfer € 2555.00  using the following bank information: 
Blender Institute B.V.
BIC: ABNANL2AXXX
IBAN: NL73137891867707439
Reference: A3J6VC3F99Q5 Invoice
Blender Institute B.V.
Buikslotermeerplein 161
1025 ET Amsterdam, The Netherlands
VATIN: NL818152011B01'''  # noqa: W291

expected_invoice_text_full_refund = '''Invoice number:
ST-1234
Date issued:
2024-05-27
Date paid:
2024-05-24
Payment method:
VISA credit card ending in 4242
Date refunded:
2024-05-24Billing Address:
admin@blender.org
Jane Doe
Some Street 12
Amsterdam 1111AB
Netherlands
Some Company B.V.
VATIN: NL123456789B12
Products Price Quantity Total Price
BCON24 Full Ticket € 365.00 3 € 1095.00
Subtotal (incl. VAT) € 1095.00
Refunded -€ 1095.00
Total € 0.00 Invoice
Blender Institute B.V.
Buikslotermeerplein 161
1025 ET Amsterdam, The Netherlands
VATIN: NL818152011B01'''  # noqa: W291

expected_invoice_text_partial_refund = '''Invoice number:
ST-4321
Date issued:
2024-05-27
Date paid:
2024-05-28
Payment method:
DEUTDE2H via GIROPAY
Date refunded:
2024-05-28Billing Address:
admin@blender.org
Jane Doe
Some Street 12
Amsterdam 1111AB
Netherlands
Some Company B.V.
VATIN: NL123456789B12
Products Price Quantity Total Price
BCON24 Full Ticket € 365.00 3 € 1095.00
Subtotal (incl. VAT) € 1095.00
Discount -€ 170.00
Refunded -€ 465.00
Total € 460.00 Invoice
Blender Institute B.V.
Buikslotermeerplein 161
1025 ET Amsterdam, The Netherlands
VATIN: NL818152011B01'''  # noqa: W291


@override_settings(SYSTEM_USER_ID=1, ACTIVE_PAYMENT_BACKEND='stripe')
@freeze_time('2024-05-27T18:12:20+01:00')
class TestStripeWebhook(TestCase):
    maxDiff = None
    webhook_url = reverse('tickets:webhook-stripe')

    def setUp(self):
        responses.start()
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml'
        )
        responses.assert_all_requests_are_fired = False

        SiteSettingsFactory(site_id=1)
        self.user = UserFactory(id=5791)
        create_system_user()
        super().setUp()

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    def _extract_text_from_pdf(self, response):
        pdf = PdfReader(BytesIO(response.content))
        self.assertEqual(1, len(pdf.pages))
        pdf_page = pdf.pages[0]
        return pdf_page.extract_text().strip()

    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    def test_checkout_session_completed_creates_a_task(self, _):
        self.assertEqual(Task.objects.count(), 0)
        tickets_count_before = Ticket.objects.count()

        payload = {
            'type': 'checkout.session.completed',
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716461495,
            'data': {
                'object': {
                    'id': 'cs_test_b1OqreGHh1JYkvXvaLXSGQRmoWhXzf09IigkalUHFXZ5JfPZygYxEhePdr',
                    'object': 'checkout.session',
                }
            },
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(tickets_count_before, Ticket.objects.count())
        self.assertEqual(Task.objects.count(), 1)

    # @_recorder.record(file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml')
    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_checkout_session_completed',
        new=tickets.tasks.handle_checkout_session_completed.task_function,
    )
    @patch(
        'tickets.tasks.update_profile_from_ticket_order',
        new=tickets.tasks.update_profile_from_ticket_order.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tickets.tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tickets.tasks.send_mail_confirm_tickets.task_function,
    )
    def test_checkout_session_completed_creates_a_paid_ticket(self, _):
        product = ProductFactory(
            name='BCON24 Full Ticket',
            url='https://dashboard.stripe.com/test/prices/price_1PFymTGYEXrrLokq2YDfZBhw',
            price=365.00,
            taxes=[
                {"amount": 3167, "description": "Entry", "rate": 21},
                {"amount": 1507, "description": "Food", "rate": 9},
            ],
        )
        tickets_count_before = Ticket.objects.count()

        payload = {
            'type': 'checkout.session.completed',
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716461495,
            'data': {
                'object': {
                    'id': 'cs_test_b1OqreGHh1JYkvXvaLXSGQRmoWhXzf09IigkalUHFXZ5JfPZygYxEhePdr',
                    'object': 'checkout.session',
                }
            },
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(tickets_count_before + 1, Ticket.objects.count())
        ticket = Ticket.objects.get(order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP')
        self.assertEqual(ticket.quantity, 7)
        self.assertEqual(ticket.refund_status, '')
        ticket = tickets.models.Ticket.objects.last()
        self.assertEqual(ticket.product, product)

        invoice_url = reverse('tickets:invoice-pdf', kwargs={'ticket_token': ticket.token})
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 302)
        self.client.force_login(self.user)
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 200)
        pdf_text = self._extract_text_from_pdf(response)
        self.assertEqual(
            expected_invoice_text_with_vat.format(
                city='Amsterdam',
                created='2024-05-27',
                email='jane.doe@example.org',
                name='Jane Doe',
                order_number=ticket.order_number,
                postal_code='1111AB',
            ),
            pdf_text,
            pdf_text,
        )
        # Check that profile was updated
        profile = ticket.user.profile
        self.assertEqual(profile.full_name, 'Jane Doe')
        self.assertEqual(profile.get_country_display(), 'Netherlands')

        # Check that confirmation email was sent
        self.assertEqual(len(mail.outbox), 1, [_.subject for _ in mail.outbox])
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Tickets payment confirmed'
        )
        self.assertNotEqual(ticket.claim_url, '')
        self.assertIn(ticket.claim_url, email.body)
        self.assertIn(ticket.claim_url, alt0_body)
        self.assertIn('ST-1', email.body)
        self.assertIn('ST-1', alt0_body)
        self.assertIn('€\xa02555.00', email.body)
        self.assertIn('€\xa02555.00', alt0_body)
        self.assertIn(ticket.sku, email.body)
        self.assertIn(ticket.sku, alt0_body)
        self.assertIn('Number of tickets:  7', email.body)
        self.assertIn('Number of tickets: 7', alt0_body)

    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_checkout_session_completed',
        new=tickets.tasks.handle_checkout_session_completed.task_function,
    )
    @patch(
        'tickets.tasks.update_profile_from_ticket_order',
        new=tickets.tasks.update_profile_from_ticket_order.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tickets.tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tickets.tasks.send_mail_confirm_tickets.task_function,
    )
    def test_checkout_session_completed_marks_ticket_paid_sends_mail(self, _):
        product = ProductFactory(
            slug='BC241DAY',
            name='BCON24 One Day Ticket',
            url='https://dashboard.stripe.com/test/prices/price_1PFymTGYEXrrLokq2YDfZBhw',
            price=365.00,
        )
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            is_paid=False,
            # Set quantity to 1 to test that ticket is automatically claimed when paid
            quantity=1,
            product=product,
            user=self.user,
        )
        tickets_count_before = Ticket.objects.count()

        payload = {
            'type': 'checkout.session.async_payment_succeeded',
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716461495,
            'data': {
                'object': {
                    'id': 'cs_test_b1OqreGHh1JYkvXvaLXSGQRmoWhXzf09IigkalUHFXZ5JfPZygYxEhePdr',
                    'object': 'checkout.session',
                }
            },
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(tickets_count_before, Ticket.objects.count())
        ticket = Ticket.objects.get(order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP')
        self.assertEqual(ticket.quantity, 1)
        self.assertEqual(ticket.refund_status, '')
        ticket = tickets.models.Ticket.objects.last()
        self.assertEqual(ticket.product, product)

        # Check ticket confirmation email
        self.assertEqual(len(mail.outbox), 2, [_.subject for _ in mail.outbox])
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket payment confirmed'
        )
        self.assertIn('ST-1', email.body)
        self.assertIn('ST-1', alt0_body)
        self.assertIn('€\xa02555.00', email.body)
        self.assertIn('€\xa02555.00', alt0_body)
        self.assertIn(product.name, email.body)
        self.assertIn(product.name, alt0_body)
        self.assertNotIn('Number of tickets', email.body)
        self.assertNotIn('Number of tickets', alt0_body)
        self.assertEqual(ticket.claim_url, '')

        # Check payment confirmation email
        email = mail.outbox[1]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket confirmed'
        )
        self.assertIn('ST-1', email.body)
        self.assertIn('ST-1', alt0_body)
        self.assertIn(product.name, email.body)
        self.assertIn('BCON24 One Day Ticket', alt0_body)

    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_checkout_session_completed',
        new=tickets.tasks.handle_checkout_session_completed.task_function,
    )
    @patch(
        'tickets.tasks.update_profile_from_ticket_order',
        new=tickets.tasks.update_profile_from_ticket_order.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tickets.tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tickets.tasks.send_mail_confirm_tickets.task_function,
    )
    def test_checkout_session_completed_marks_ticket_paid_sends_mail_with_claim_url_because_multiple(
        self, _
    ):
        product = ProductFactory(
            slug='BC241DAY',
            name='BCON24 One Day Ticket',
            url='https://dashboard.stripe.com/test/prices/price_1PFymTGYEXrrLokq2YDfZBhw',
            price=365.00,
        )
        ticket = TicketFactory(
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            is_paid=False,
            quantity=3,
            product=product,
            user=self.user,
        )
        tickets_count_before = Ticket.objects.count()

        payload = {
            'type': 'checkout.session.async_payment_succeeded',
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716461495,
            'data': {
                'object': {
                    'id': 'cs_test_b1OqreGHh1JYkvXvaLXSGQRmoWhXzf09IigkalUHFXZ5JfPZygYxEhePdr',
                    'object': 'checkout.session',
                }
            },
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(tickets_count_before, Ticket.objects.count())
        ticket = Ticket.objects.get(order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP')
        self.assertEqual(ticket.quantity, 3)
        self.assertEqual(ticket.refund_status, '')
        ticket = tickets.models.Ticket.objects.last()
        self.assertEqual(ticket.product, product)

        # Check payment confirmation email
        self.assertEqual(len(mail.outbox), 1, [_.subject for _ in mail.outbox])
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Tickets payment confirmed'
        )
        self.assertIn(str(ticket.token), ticket.claim_url)
        self.assertIn(str(ticket.token), email.body)
        self.assertIn(str(ticket.token), alt0_body)
        self.assertIn(ticket.claim_url, email.body)
        self.assertIn(ticket.claim_url, alt0_body)
        self.assertIn('ST-1', email.body)
        self.assertIn('ST-1', alt0_body)
        self.assertIn('€\xa02555.00', email.body)
        self.assertIn('€\xa02555.00', alt0_body)
        self.assertIn(product.name, email.body)
        self.assertIn(product.name, alt0_body)
        self.assertIn('Number of tickets:  3', email.body)
        self.assertIn('Number of tickets: 3', alt0_body)
        self.assertIn('Unclaimed tickets:  3', email.body)
        self.assertIn('Unclaimed tickets: 3', alt0_body)

    # @_recorder.record(file_path='tickets/tests/stripe_retrieve_session_for_payment_intent_fully_refunded.yaml')
    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_charge_refunded',
        new=tickets.tasks.handle_charge_refunded.task_function,
    )
    def test_charge_refunded_full_clears_attendees(self, _):
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent_fully_refunded.yaml'
        )
        ticket = TicketFactory(
            order_number='ST-1234',
            order_id='pi_3PK05DGYEXrrLokq0ih0Ydmr',
            is_paid=True,
            quantity=3,
        )
        ticket.attendees.add(UserFactory())
        ticket.attendees.add(UserFactory())
        self.assertEqual(ticket.attendees.count(), 2)

        payload = {
            "id": "evt_3PK05DGYEXrrLokq09RXTFFx",
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716565109,
            'data': {
                'object': {
                    'id': 'ch_3PK05DGYEXrrLokq0T0x4j7i',
                    'object': 'charge',
                    'amount': 109500,
                    'amount_captured': 109500,
                    'amount_refunded': 109500,
                    'captured': True,
                    'created': 1716892767,
                    'currency': 'eur',
                    'livemode': False,
                    'paid': True,
                    'payment_intent': 'pi_3PK05DGYEXrrLokq0ih0Ydmr',
                    'payment_method': 'pm_1PK05DGYEXrrLokqqM5hSqYq',
                    'refunded': False,
                    'status': 'succeeded',
                },
            },
            'livemode': False,
            'type': 'charge.refunded',
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.attendees.count(), 0)
        ticket = Ticket.objects.get(id=ticket.id)
        self.assertEqual(ticket.is_paid, False)

        # Check that invoice PDF includes refund date and updated total
        invoice_url = reverse('tickets:invoice-pdf', kwargs={'ticket_token': ticket.token})
        self.client.force_login(ticket.user)
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 200)
        pdf_text = self._extract_text_from_pdf(response)
        self.assertEqual(expected_invoice_text_full_refund, pdf_text, pdf_text)

    # @_recorder.record(file_path='tickets/tests/stripe_retrieve_session_for_payment_intent_partially_refunded.yaml')
    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_charge_refunded',
        new=tickets.tasks.handle_charge_refunded.task_function,
    )
    def test_charge_refunded_partial_does_not_clear_attendies_updated_total_shown_in_invoice(
        self, _
    ):
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent_partially_refunded.yaml'
        )
        ticket = TicketFactory(
            order_number='ST-4321',
            user=self.user,
            order_id='pi_3PLNQZGYEXrrLokq1ap2ZZlG',
            is_paid=True,
            quantity=3,
        )
        ticket.attendees.add(UserFactory())
        ticket.attendees.add(UserFactory())
        self.assertEqual(ticket.attendees.count(), 2)

        payload = {
            'id': 'evt_3PLNQZGYEXrrLokq1q4IrIOu',
            'object': 'event',
            'api_version': '2022-11-15',
            'created': 1716894223,
            'data': {
                'object': {
                    'id': 'py_3PLNQZGYEXrrLokq1fDlImot',
                    'object': 'charge',
                    'amount': 92500,
                    'amount_captured': 92500,
                    'amount_refunded': 46500,
                    'captured': True,
                    'created': 1716892767,
                    'currency': 'eur',
                    'livemode': False,
                    'on_behalf_of': None,
                    'order': None,
                    'outcome': {
                        'network_status': 'approved_by_network',
                        'reason': None,
                        'risk_level': 'not_assessed',
                        'seller_message': 'Payment complete.',
                        'type': 'authorized',
                    },
                    'paid': True,
                    'payment_intent': 'pi_3PLNQZGYEXrrLokq1ap2ZZlG',
                    'payment_method': 'pm_1PLNQZGYEXrrLokqTll3RwAt',
                    'refunded': False,
                    'status': 'succeeded',
                },
            },
            'livemode': False,
            'type': 'charge.refunded',
        }
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ticket.attendees.count(), 2)

        invoice_url = reverse('tickets:invoice-pdf', kwargs={'ticket_token': ticket.token})
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 302)
        self.client.force_login(self.user)
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 200)
        pdf_text = self._extract_text_from_pdf(response)
        self.assertEqual(expected_invoice_text_partial_refund, pdf_text, pdf_text)

    @patch('tickets.stripe_utils.stripe.webhook.WebhookSignature.verify_header', return_value=True)
    @patch(
        'tickets.views.webhooks_stripe.handle_payment_intent_requires_action',
        new=tickets.tasks.handle_payment_intent_requires_action.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_bank_transfer_required',
        new=tickets.tasks.send_mail_bank_transfer_required.task_function,
    )
    @patch(
        'tickets.tasks.update_profile_from_ticket_order',
        new=tickets.tasks.update_profile_from_ticket_order.task_function,
    )
    def test_payment_intent_requires_action_creates_pending_ticket(self, _):
        product = ProductFactory(
            slug='BCON24E',
            name='BCON24 Full Ticket - Early Bird',
            url='https://dashboard.stripe.com/test/prices/price_1PFymTGYEXrrLokq2YDfZBhw',
            price=365.00,
        )
        tickets_count_before = tickets.models.Ticket.objects.count()

        file = 'tickets/tests/stripe_payment_intent.requires_action.json'
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent_next_action.yaml'
        )
        with open(file) as f:
            payload = json.load(f)
        response = self.client.post(
            self.webhook_url,
            data=payload,
            content_type='application/json',
            HTTP_STRIPE_SIGNATURE='wehavemonkeypatchedthesignaturechecker',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(tickets.models.Ticket.objects.count(), tickets_count_before + 1)
        ticket = tickets.models.Ticket.objects.get(order_id=payload['data']['object']['id'])
        self.assertEqual(ticket.sku, 'BCON24E')
        self.assertEqual(ticket.order_number, 'ST-1')
        self.assertEqual(ticket.product, product)
        self.assertFalse(ticket.is_paid)
        self.assertEqual(ticket.quantity, 7)

        # Check that invoice PDF includes instructions
        invoice_url = reverse('tickets:invoice-pdf', kwargs={'ticket_token': ticket.token})
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 302)
        self.client.force_login(self.user)
        response = self.client.get(invoice_url)
        self.assertEqual(response.status_code, 200)
        pdf_text = self._extract_text_from_pdf(response)
        self.assertEqual(
            expected_invoice_text_with_bank_transfer_instructions.format(
                city='Amsterdam',
                created='2024-05-27',
                email='jane.doe@example.org',
                name='Jane Doe',
                order_number=ticket.order_number,
                postal_code='1111AB',
            ),
            pdf_text,
            pdf_text,
        )

        # Check that the same instructions were sent via email
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(email.subject, 'Blender Conference: Bank Transfer Instructions')
        self.assertNotIn(ticket.claim_url, email.body)
        self.assertNotIn(ticket.claim_url, alt0_body)
        self.assertIn('ST-1', email.body)
        self.assertIn('ST-1', alt0_body)
        self.assertIn('A3J6VC3F99Q5', email.body)
        self.assertIn('A3J6VC3F99Q5', alt0_body)
        self.assertIn('IBAN: NL73137891867707439', email.body)
        self.assertIn('IBAN: NL73137891867707439', alt0_body)
        self.assertIn('BIC: ABNANL2AXXX', email.body)
        self.assertIn('BIC: ABNANL2AXXX', alt0_body)
        self.assertNotIn('$\xa02555.00', email.body)
        self.assertNotIn('$\xa02555.00', alt0_body)
        self.assertIn(product.name, email.body)
        self.assertIn('BCON24 Full Ticket - Early Bird', alt0_body)
