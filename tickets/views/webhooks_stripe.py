"""Defines webhook views."""
import json
import logging

from django.conf import settings
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import stripe

from tickets.tasks import (
    handle_charge_refunded,
    handle_checkout_session_completed,
    handle_payment_intent_requires_action,
)
from tickets.stripe_utils import construct_event_from_request

logger = logging.getLogger(__name__)


@method_decorator(csrf_exempt, name='dispatch')
class StripeWebhookView(View):
    """Handle Stripe events arriving via a webhook."""

    def post(self, request, *args, **kwargs):
        """Handle Stripe webhook request: only POST is expected here."""
        if 'HTTP_STRIPE_SIGNATURE' not in request.META:
            logger.error('Missing signature')
            return HttpResponse(status=401)

        try:
            event = construct_event_from_request(request)
        except ValueError as e:
            logger.error('Error parsing payload: {}'.format(str(e)))
            return HttpResponse(status=400)
        except stripe.error.SignatureVerificationError as e:
            logger.error('Error verifying webhook signature: {}'.format(str(e)))
            return HttpResponse(status=400)

        # if not settings.TESTING:
        #    with open(f'stripe_{event.type}_{event.data.object.id}.json', 'wb+') as f:
        #        f.write(request.body)
        logger.info('Processing event type=%s for object_id=%s', event.type, event.data.object.id)
        if event.type == 'checkout.session.completed':
            if settings.DEBUG:
                handle_checkout_session_completed.task_function(json.loads(json.dumps(event)))
            else:
                # Delay handling of this event to avoid racing with the synchronous success page
                handle_checkout_session_completed(event, schedule=30)
        if event.type == 'checkout.session.async_payment_succeeded':
            if settings.DEBUG:
                handle_checkout_session_completed.task_function(json.loads(json.dumps(event)))
            else:
                handle_checkout_session_completed(event)
        elif event.type == 'charge.refunded':
            if settings.DEBUG:
                handle_charge_refunded.task_function(json.loads(json.dumps(event)))
            else:
                handle_charge_refunded(event)
        elif event.type == 'payment_intent.requires_action':
            if settings.DEBUG:
                handle_payment_intent_requires_action.task_function(json.loads(json.dumps(event)))
            else:
                handle_payment_intent_requires_action(event)
        else:
            logger.info('Unhandled event type {}'.format(event.type))

        return HttpResponse(status=200)
