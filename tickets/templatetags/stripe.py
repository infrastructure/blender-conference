import logging

from django import template

import tickets.stripe_utils as stripe_utils

register = template.Library()
logger = logging.getLogger(__name__)


@register.filter
def format_stripe_payment_method(payment_method: dict) -> str:
    """Format a given payment method as returned by Stripe API."""
    return stripe_utils.format_payment_method(payment_method)


@register.filter
def format_custom_field(field: dict) -> str:
    """Format a given custom field set during checkout."""
    try:
        label = field['label']['custom']
        dropdown = field['dropdown']
        value = dropdown['value']
        value_label = next(_['label'] for _ in dropdown['options'] if _['value'] == value)
        return f'{label} {value_label}'
    except Exception:
        logger.exception(f'Unable to format custom field: {field}')
    return ''


@register.filter
def format_custom_field_admin(field: dict) -> str:
    """Format a given custom field set during checkout to be displayed in the admin."""
    try:
        dropdown = field['dropdown']
        value = dropdown['value']
        value_label = next(_['label'] for _ in dropdown['options'] if _['value'] == value)
        return f'{value_label}'
    except Exception:
        logger.exception(f'Unable to format custom field: {field}')
    return ''
