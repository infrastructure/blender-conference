from unittest.mock import patch

from django.core import mail
from django.test import TestCase, override_settings

# from responses import _recorder
import responses

from tickets.tests.factories import (
    SiteSettingsFactory,
    TicketFactory,
    UserFactory,
    create_system_user,
)
from tickets.tests.graphql import mock_saleor_responses
import tickets.tasks as tasks


@override_settings(SYSTEM_USER_ID=1, ACTIVE_PAYMENT_BACKEND='saleor')
class TestTicketModel(TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        SiteSettingsFactory(site_id=1)
        create_system_user()

    @responses.activate
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    @patch(
        'tickets.tasks.generate_invoice',
        new=tasks.generate_invoice.task_function,
    )
    def test_ticket_changed_to_is_paid_mail_sent(self):
        mock_saleor_responses()
        ticket = TicketFactory(
            quantity=3, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=False
        )

        ticket.is_paid = True
        ticket.save(update_fields={'is_paid': True})

        # Check that confirmation email was sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Tickets payment confirmed'
        )

    def test_ticket_claim_url_not_empty_if_not_all_claimed(self):
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=True
        )

        self.assertEqual(ticket.unclaimed, 2)
        self.assertEqual(ticket.claim_url, f'/tickets/claim/{ticket.token}/')

    def test_ticket_claim_url_empty_if_all_claimed(self):
        ticket = TicketFactory(
            quantity=2, order_token='8ef27213-845a-4bb9-a917-d342b7566a1c', is_paid=True
        )
        attendee1 = UserFactory()
        attendee2 = UserFactory()
        ticket.attendees.add(attendee1, attendee2)
        self.assertTrue(ticket.is_claimed_by(attendee1.pk))
        self.assertTrue(ticket.is_claimed_by(attendee2.pk))

        self.assertEqual(ticket.unclaimed, 0)
        self.assertEqual(ticket.claim_url, '')

    def test_invoice_download_url_empty(self):
        ticket = TicketFactory()

        self.assertEqual(ticket.invoice_download_url, '')

    def test_invoice_download_url_automatically_generated_invoice_url(self):
        ticket = TicketFactory(invoice_url='https://staging.shop.blender.org/invoices/foo-bar.pdf')

        self.assertEqual(
            ticket.invoice_download_url,
            'https://staging.shop.blender.org/invoices/foo-bar.pdf',
        )

    def test_invoice_download_url_file(self):
        ticket = TicketFactory(invoice='/invoices/custom-invoice.pdf')

        self.assertEqual(
            ticket.invoice_download_url,
            '/media/invoices/custom-invoice.pdf',
        )

    def test_invoice_download_url_file_takes_precedence_over_url(self):
        ticket = TicketFactory(
            invoice_url='https://staging.shop.blender.org/invoices/foo-bar.pdf',
            invoice='/invoices/custom-invoice.pdf',
        )

        self.assertEqual(
            ticket.invoice_download_url,
            '/media/invoices/custom-invoice.pdf',
        )

    def test_ticket_admin_order_url(self):
        ticket_order_id = 'ABC123'
        ticket = TicketFactory(order_id=ticket_order_id)
        self.assertEqual(
            ticket.get_admin_order_url(), ''
        )  # Saleor has been retired: these pages no longer exist


@override_settings(SYSTEM_USER_ID=1, ACTIVE_PAYMENT_BACKEND='stripe')
class TestTicketIfStripeModel(TestCase):
    maxDiff = None

    def setUp(self):
        responses.start()
        # Load previously recorded Stripe responses from YAML files
        responses._add_from_file(
            file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml'
        )

        SiteSettingsFactory(site_id=1)
        create_system_user()
        super().setUp()

    def tearDown(self):
        super().tearDown()
        responses.stop()
        responses.reset()

    # @_recorder.record(file_path='tickets/tests/stripe_retrieve_session_for_payment_intent.yaml')
    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    def test_ticket_changed_to_is_paid_mail_sent_multiple_tickets(self):
        ticket = TicketFactory(
            is_paid=False,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            quantity=10,
            sku='BCON22 One Day',
        )

        ticket.is_paid = True
        ticket.save(update_fields={'is_paid'})

        # Check that confirmation email was sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Tickets payment confirmed'
        )
        self.assertIn(ticket.claim_url, email.body)
        self.assertIn(ticket.claim_url, alt0_body)
        self.assertIn('ST-1234', email.body)
        self.assertIn('ST-1234', alt0_body)
        self.assertIn('€\xa02555.00', email.body)
        self.assertIn('€\xa02555.00', alt0_body)
        self.assertIn(ticket.sku, email.body)
        self.assertIn(ticket.sku, alt0_body)
        self.assertIn('Number of tickets:  10', email.body)
        self.assertIn('Number of tickets: 10', alt0_body)

    @patch(
        'tickets.tasks.send_mail_tickets_paid',
        new=tasks.send_mail_tickets_paid.task_function,
    )
    @patch(
        'tickets.tasks.send_mail_confirm_tickets',
        new=tasks.send_mail_confirm_tickets.task_function,
    )
    def test_ticket_changed_to_is_paid_mail_sent(self):
        ticket = TicketFactory(
            is_paid=False,
            order_id='pi_3PJZF7GYEXrrLokq3RrJnYAP',
            order_number='ST-1234',
            quantity=1,
            sku='BCON22 One Day',
        )
        ticket.attendees.add(ticket.user)

        ticket.is_paid = True
        ticket.save(update_fields={'is_paid'})

        # Check that confirmation email was sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        alt0_body, alt0_type = email.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')
        self.assertEqual(email.to, [ticket.user.email])
        self.assertEqual(email.from_email, 'Blender Conference <conference@blender.org>')
        self.assertEqual(
            email.subject, f'Blender Conference {ticket.edition.year}: Ticket confirmed'
        )
        self.assertIn(ticket.claim_url, email.body)
        self.assertIn(ticket.claim_url, alt0_body)
        self.assertIn('ST-1234', email.body)
        self.assertIn('ST-1234', alt0_body)
        self.assertNotIn('$\xa01000.00', email.body)
        self.assertNotIn('$\xa01000.00', alt0_body)
        self.assertIn(ticket.sku, email.body)
        self.assertIn(ticket.sku, alt0_body)
        ticket_url = ticket.get_absolute_url()
        self.assertIn(ticket_url, email.body)
        self.assertIn(ticket_url, alt0_body)
