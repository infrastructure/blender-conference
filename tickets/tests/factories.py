from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory
import factory

from blender_id_oauth_client.models import OAuthUserInfo, OAuthToken

import conference_main.models
import tickets.models

User = get_user_model()
# FIXME(anna): user-related factories should probably move to a more common location


class OAuthUserInfoFactory(DjangoModelFactory):
    class Meta:
        model = OAuthUserInfo

    oauth_user_id = factory.Sequence(lambda n: n + 899999)
    user = factory.SubFactory('tickets.tests.factories.UserFactory')


class OAuthUserTokenFactory(DjangoModelFactory):
    class Meta:
        model = OAuthToken

    oauth_user_id = factory.Sequence(lambda n: n + 899999)
    user = factory.SubFactory('tickets.tests.factories.UserFactory')


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    id = factory.Sequence(lambda n: n)

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.LazyAttribute(lambda o: f'{o.first_name}_{o.last_name}')
    email = factory.LazyAttribute(
        lambda a: '{}.{}@example.com'.format(a.first_name, a.last_name).lower()
    )
    password = 'pass'

    oauth_tokens = factory.RelatedFactoryList(OAuthUserTokenFactory, factory_related_name='user')
    oauth_info = factory.RelatedFactory(OAuthUserInfoFactory, factory_related_name='user')


class EditionFactory(DjangoModelFactory):
    class Meta:
        model = conference_main.models.Edition

    year = factory.Sequence(lambda n: 2022 + n)
    path = factory.LazyAttribute(lambda o: f'{o.year}')
    title = factory.LazyAttribute(lambda o: f'Blender Conference {o.year}')


class SiteSettingsFactory(DjangoModelFactory):
    class Meta:
        model = conference_main.models.SiteSettings

    current_edition = factory.SubFactory('tickets.tests.factories.EditionFactory')


class TicketFactory(DjangoModelFactory):
    class Meta:
        model = tickets.models.Ticket

    sku = 'BC22'
    edition = factory.SubFactory('tickets.tests.factories.EditionFactory')
    user = factory.SubFactory('tickets.tests.factories.UserFactory')


class ProductFactory(DjangoModelFactory):
    class Meta:
        model = tickets.models.Product


def create_system_user() -> User:
    """Create the system user used for additional logging."""
    system_user = UserFactory(id=1, email='system@blender.org')
    # Reset ID sequence to avoid clashing with an already used ID 1
    UserFactory.reset_sequence(100, force=True)
    return system_user
