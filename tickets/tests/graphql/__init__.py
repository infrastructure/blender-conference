import json
import os.path

from django.conf import settings
import responses


def _read_graphql_fixture(name):
    cur_dir = os.path.dirname(__file__)
    fixture_file = os.path.join(cur_dir, f'fixtures/{name}.json')
    with open(fixture_file, 'r') as f:
        return f.read()


def graphql_callback(quantity=None, is_paid=None, checkout_data=None):
    def _response_callback(request):
        payload = json.loads(request.body)
        headers = {}
        body = '{}'
        token = '01e5ffc2-41e8-46ad-a4e0-b74eea517cef'
        br = '\n'
        if 'query' in payload:
            query = payload['query']
            if 'productVariant(channel: "default-channel", sku: "BC22")' in query:
                response = _read_graphql_fixture('productVariant')
                body = f'{{ "data": {response} }}'
            elif (
                f'mutation {{{br}  checkoutCreate({br}    input: {{channel: "default-channel"'
                in query
            ):
                response = _read_graphql_fixture('checkoutCreate')
                body = f'{{ "data": {response} }}'
            elif f'checkout(token: "{token}")' in query:
                response = _read_graphql_fixture('checkout')
                data = json.loads(response)
                if checkout_data:
                    data['checkout'].update(checkout_data)
                body = json.dumps({'data': data})
            elif f'mutation {{{br}  updateMetadata({br}    id: "{token}"' in query:
                body = '{ "data": {"updateMetadata": {"errors": [], "item": {"metadata": []}}} }'
            elif f'mutation {{{br}  checkoutPaymentCreate({br}    token: "{token}"' in query:
                response = _read_graphql_fixture('checkoutPaymentCreate')
                body = f'{{ "data": {response} }}'
            elif f'mutation {{{br}  checkoutComplete(token: "{token}")' in query:
                response = _read_graphql_fixture('checkoutComplete')
                data = json.loads(response)
                if quantity:
                    data['checkoutComplete']['order']['lines'][0]['quantity'] = quantity
                if is_paid is not None:
                    data['checkoutComplete']['order']['isPaid'] = is_paid
                body = json.dumps({'data': data})
            elif 'query IntrospectionQuery' in query:
                body = _read_graphql_fixture('introspectionQuery')
            elif 'orderByToken(token: "8ef27213-845a-4bb9-a917-d342b7566a1c")' in query:
                response = _read_graphql_fixture('orderByToken')
                body = f'{{ "data": {response} }}'
            elif query.startswith('{\n  order(id: "'):
                response = _read_graphql_fixture('order')
                body = f'{{ "data": {response} }}'
            elif 'mutation {\n  invoiceRequest(' in query:
                response = _read_graphql_fixture('invoiceRequest')
                body = f'{{ "data": {response} }}'
            elif 'mutation {\n  checkoutEmailUpdate(\n' in query:
                response = json.loads(_read_graphql_fixture('checkout'))
                response['errors'] = []
                body = f'{{ "data": {{"checkoutEmailUpdate": {json.dumps(response)} }} }}'
            else:
                raise Exception(f'No response for "{payload}"')
        return (200, headers, body)

    return _response_callback


def mock_saleor_responses(**kwargs) -> None:
    """Set up mock responses of Saleor GraphQL API."""
    base_url = settings.SALEOR_API_URL

    responses.add_callback(
        responses.POST,
        base_url,
        callback=graphql_callback(**kwargs),
        content_type="application/json",
    )


def _clean_gq_calls(responses):
    # Skip IntrospectionQuery which might or might not be part of requests
    return [_ for _ in responses.calls if b'IntrospectionQuery' not in _.request.body]
