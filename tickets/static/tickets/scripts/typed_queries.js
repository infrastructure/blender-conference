window.TypedQueries = (function() {
  function TypedQueries() {
    const metadataType = '{key value}';
    const moneyType = '{amount currency}';
    const taxedMoneyType = `{
      currency
      gross ${moneyType}
      net ${moneyType}
      tax ${moneyType}
    }`;
    const priceRangeType = `{
      start ${taxedMoneyType}
      stop ${taxedMoneyType}
    }`;
    const checkoutErrorType = `{
      field
      message
      code
      variants
      lines
      addressType
    }`;
    const productTypeType = `{
      id
      name
      slug
    }`;
    const selectedAttributeType = `{
      attribute {
        name
        slug
        type
        unit
      }
      values {
        name
        slug
        value
        inputType
        richText
        file {url}
        boolean
        date
        dateTime
      }
    }`;
    const productType = `
      id
      name
      slug
      description
      chargeTaxes
      rating
      channel
      attributes ${selectedAttributeType}
      productType ${productTypeType}
      category {
        id
        seoTitle
        seoDescription
        slug
        name
        description
      }
      thumbnail {
        url
        alt
      }
      pricing {
        onSale
        priceRange ${priceRangeType}
        priceRangeUndiscounted ${priceRangeType}
        priceRangeLocalCurrency ${priceRangeType}
      }
      isAvailable
      taxType {
        description
        taxCode
      }

      media{
        id
        sortOrder
        alt
        type
        oembedData
        url
      }
      availableForPurchase
      isAvailableForPurchase
    `;
    const productVariantType = `
      id
      metadata ${metadataType}
      name
      sku
      trackInventory
      attributes ${selectedAttributeType}
      quantityLimitPerCustomer
      channel
      pricing {
        price ${taxedMoneyType}
      }
      margin
      media {
        id
        sortOrder
        alt
        type
        oembedData
        url
      }
      quantityAvailable
    `;
    const userType = `{
      id
      email
      firstName
      lastName
      isStaff
      isActive
      checkoutTokens
      note
      languageCode
    }`;
    const checkoutLineType = `{
      id
      variant {
        ${productVariantType}
        product {${productType}}
      }
      quantity
      totalPrice ${taxedMoneyType}
      requiresShipping
    }`;
    const addressType = `{
      firstName
      lastName
      companyName
      streetAddress1
      streetAddress2
      city
      cityArea
      phone
      postalCode
      countryArea
      country {
        code
        country
        vat {
          countryCode
          standardRate
          reducedRates {rate rateType}
        }
      }
    }`;
    const checkoutType = `{
      user ${userType}
      metadata ${metadataType}
      metafields
      channel {slug}
      billingAddress ${addressType}
      note
      discount ${moneyType}
      discountName
      voucherCode
      email
      quantity
      lines ${checkoutLineType}
      subtotalPrice ${taxedMoneyType}
      totalPrice ${taxedMoneyType}
      token
      languageCode
      availablePaymentGateways {
        id
        name
        config {
          field
          value
        }
      }
    }`;

    const checkoutResultsType = `{
      checkout ${checkoutType}
      errors ${checkoutErrorType}
    }`;

    function escapeQuotes(value) {
      return value.replace('\"', '\\"');
    }

    function getCheckoutBillingAddressUpdateMutation(token, data) {
      // validationRules are a new preview feature in 3.5
      return Apollo.gql`mutation {
        checkoutBillingAddressUpdate(
          validationRules: {
            checkRequiredFields: false
            checkFieldsFormat: true
            enableFieldsNormalization: true
          }
          billingAddress: {
            country: ${data.country}
            companyName: "${escapeQuotes(data.companyName)}"
            firstName: "${escapeQuotes(data.firstName)}"
            lastName: "${escapeQuotes(data.lastName)}"
            streetAddress1: "${escapeQuotes(data.streetAddress1)}"
            postalCode: "${data.postalCode}"
            city: "${data.city}"
            countryArea: "${data.countryArea}"
          }
          token: "${token}"
        ) {
          checkout ${checkoutType}
          errors {
            field
            message
            code
          }
        }
      }`;
    }

    function getProductVariantBySKUQuery(channel, sku) {
      return Apollo.gql`{
        productVariant(channel: "${channel}", sku: "${sku}") {
          ${productVariantType}
          product {${productType}}
        }
      }`;
    }

    function getProductQuery(channel, productId) {
      return Apollo.gql`{
        product(channel: "${channel}", id: "${productId}") {
          ${productType}
          variants {${productVariantType}}
        }
      }`;
    }

    function getProductsQuery(channel) {
      return Apollo.gql`{
        products(first: 1, channel: "${channel}") {
          edges {
            node {
              ${productType}
              variants {${productVariantType}}
            }
          }
        }
      }`;
    }

    function getCheckoutLinesUpdateMutation(token, variantId, quantity) {
      return Apollo.gql(`mutation {
        checkoutLinesUpdate(
            token: "${token}"
            lines: [{ quantity: ${quantity}, variantId: "${variantId}" }]
        ) ${checkoutResultsType}
      }`);
    }

    function getCheckoutPromoCodeMutation(token, code, mutationName) {
      return Apollo.gql(`mutation {
        ${mutationName} (
            token: "${token}"
            promoCode: "${code}"
        ) ${checkoutResultsType}
      }`);
    }

    function getCheckoutQuery(token) {
      return Apollo.gql`{
        checkout(token: "${token}") ${checkoutType}
      }`;
    }

    function getUpdateMetadata(objectId, metadata) {
      // N.B.: for Order and Checkout types it is recommended
      // to use token as the id input value, for other types use GraphQL ID.
      // Changing the Order metadata is possible only with the use of token.
      const input = JSON.stringify(metadata).replaceAll('"key"', 'key').replaceAll('"value"', 'value');
      return Apollo.gql`mutation {
        updateMetadata(
            id: "${objectId}"
            input: ${input}
        ) {
          metadataErrors {
            field
            code
            message
          }
          item {
            metadata ${metadataType}
          }
        }
      }`;
    }

    return Object.freeze({
      getCheckoutBillingAddressUpdateMutation,
      getCheckoutLinesUpdateMutation,
      getCheckoutPromoCodeMutation,
      getCheckoutQuery,
      getProductQuery,
      getProductVariantBySKUQuery,
      getProductsQuery,
      getUpdateMetadata
    });
  }
  return Object.freeze(TypedQueries);
})();
