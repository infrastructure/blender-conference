document.addEventListener('DOMContentLoaded', function() {
  function copyToClipboard(textToCopy) {
    // Navigator clipboard API needs a secure context (HTTPS)
    if (navigator.clipboard && window.isSecureContext) {
      return navigator.clipboard.writeText(textToCopy);
    } else {
      // Otherwise workaround using the text area method
      let textArea = document.createElement("textarea");
      textArea.value = textToCopy;
      // Make the textarea out of viewport
      textArea.style.position = "fixed";
      textArea.style.left = "-999999px";
      textArea.style.top = "-999999px";
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
      return new Promise((res, rej) => {
        // Here is where the magic happens:
        document.execCommand('copy') ? res() : rej();
        textArea.remove();
      });
    }
  }

  function copyClaimUrl(event) {
    console.log(event);
    const claimUrlEl = document.getElementById("claim-url");
    /* Select the text field */
    claimUrlEl.select();
    claimUrlEl.setSelectionRange(0, 99999); /* For mobile devices */
    /* Copy the text inside the text field */
    copyToClipboard(claimUrlEl.value);
  }

  const copyClaimUrlBtn = document.getElementById('btn-copy-claim-url');
  const claimUrlEl = document.getElementById('claim-url');
  if (copyClaimUrlBtn && claimUrlEl) {
    copyClaimUrlBtn.addEventListener('click', function(ev) {
      ev.preventDefault();
      ev.stopPropagation();
      copyToClipboard(claimUrlEl.value);
      copyClaimUrlBtn.classList.add('btn-success');
      setTimeout(function() {
        copyClaimUrlBtn.classList.remove('btn-success');
      }, 500);
      return false;
    });
  }
});
