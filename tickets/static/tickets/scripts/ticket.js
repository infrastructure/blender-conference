document.addEventListener('DOMContentLoaded', function() {
  const utils = new CheckoutUtils();

  const formEl = document.getElementById('new-checkout');
  const quantityEl = formEl.elements.id_quantity;
  const productPriceEl = document.getElementById('product-price');

  function handleQuantityChange(ev) {
    quantityEl.value = Math.max(parseInt(quantityEl.value), 1);
    const newPrice = {
      amount: parseInt(quantityEl.value) * productPriceEl.dataset.amount,
      currency: productPriceEl.dataset.currency
    };
    productPriceEl.innerHTML = utils.formatMoney(newPrice);
  }

  quantityEl.addEventListener('change', handleQuantityChange);
});
