"""Implements various calls to Saleor API."""
from typing import List, Dict
import json
import logging

from django.conf import settings
from django_countries import countries
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from i18naddress import load_validation_data, _load_country_data, FIELD_MAPPING

logger = logging.getLogger(__name__)
transport = RequestsHTTPTransport(url=settings.SALEOR_API_URL)
client = Client(transport=transport, fetch_schema_from_transport=True)
manage_transport = RequestsHTTPTransport(
    url=settings.SALEOR_API_URL,
    headers={'Authorization': f'Bearer {settings.SALEOR_APP_TOKEN}'},
)
manage_client = Client(transport=manage_transport, fetch_schema_from_transport=True)
GATEWAYS = {
    'bank': 'mirumee.payments.dummy',
    'braintree': 'mirumee.payments.braintree',
}


def _get_address_validation_rules():
    """Return address validation rules used to render the form.

    Use Saleor's library to load some of the address validation rules use by Saleor,
    in an attempt to make filling in the address form easier.
    """
    rules = {}
    # "Unpack" i18naddress's field mapping by inverting it for our use
    _FIELD_MAPPING = {v: k for k, v in FIELD_MAPPING.items()}
    i18naddress_validation_data = load_validation_data()
    countries_dict = dict(countries)
    for country_code, code_data in i18naddress_validation_data.items():
        if len(country_code) != 2 or country_code not in countries_dict:
            # Does not look like a country code?
            continue
        require = code_data.get('require')
        if require is None:
            # No known address validation rules (or nothing is required)
            continue
        info, country_rules = _load_country_data(country_code)
        rules[country_code] = {
            'require': {field: _FIELD_MAPPING[field] in require for field in _FIELD_MAPPING.keys()},
            'country_area_type': info['state_name_type'],
            'country_area_choices': [],
        }
        for k, v in country_rules.items():
            # Looks like i18naddress is using dashes in keys for data in other languages
            if '--' in k:
                continue
            # code/X/Y appear to be subdivisions of larger administrative areas, we don't use those
            if len(k.split('/')) > 2:
                continue
            # Same as the above
            if not k.startswith(f'{country_code}/'):
                continue
            area_country_code = v.get('key', k.split('/')[1])
            name = v.get('lname', v.get('name', v['key']))
            rules[country_code]['country_area_choices'].append((area_country_code, name))
        # Sort country area choices by their human-readable names
        rules[country_code]['country_area_choices'] = sorted(
            rules[country_code]['country_area_choices'], key=lambda _: _[1]
        )
    return rules


address_validation_rules = _get_address_validation_rules()
metadataType = '{key value}'
moneyType = '{amount currency}'
errorType = '{field message code}'
selectedAttributeType = """{
    attribute {
        name
        slug
        type
        unit
    }
    values {
        name
        slug
        value
        inputType
        richText
        file {url}
        boolean
        date
        dateTime
    }
}"""
taxedMoneyType = f"""{{
    currency
    gross {moneyType}
    net {moneyType}
    tax {moneyType}
}}"""
priceRangeType = f"""{{
    start {taxedMoneyType}
    stop {taxedMoneyType}
}}"""
productTypeType = '{id name slug}'
productVariantType = f"""
    id
    metadata {metadataType}
    name
    sku
    trackInventory
    attributes {selectedAttributeType}
    quantityLimitPerCustomer
    channel
    pricing {{
        price {taxedMoneyType}
    }}
    margin
    media {{
        id
        sortOrder
        alt
        type
        oembedData
        url
    }}
    quantityAvailable
"""
productType = f"""
    id
    name
    slug
    description
    chargeTaxes
    rating
    channel
    attributes {selectedAttributeType}
    productType {productTypeType}
    category {{
        id
        seoTitle
        seoDescription
        slug
        name
        description
    }}
    thumbnail {{ url alt }}
    pricing {{
        onSale
        priceRange {priceRangeType}
        priceRangeUndiscounted {priceRangeType}
        priceRangeLocalCurrency {priceRangeType}
    }}
    isAvailable
    taxType {{
        description
        taxCode
    }}
    media {{
        id
        sortOrder
        alt
        type
        oembedData
        url
    }}
    availableForPurchase
    isAvailableForPurchase
"""
checkoutErrorType = """{
    field
    message
    code
    variants
    lines
    addressType
}"""
userType = """{
    id
    email
    firstName
    lastName
    isStaff
    isActive
    checkoutTokens
    note
    languageCode
}"""
addressType = """{
    firstName
    lastName
    companyName
    streetAddress1
    streetAddress2
    city
    cityArea
    phone
    postalCode
    countryArea
    country {
        code
        country
        vat {
            countryCode
            standardRate
            reducedRates {rate rateType}
        }
    }
}"""
checkoutLineType = f"""{{
    id
    variant {{
        {productVariantType}
        product {{ {productType} }}
    }}
    quantity
    totalPrice {taxedMoneyType}
}}"""
checkoutType = f"""{{
    id
    user {userType}
    metadata {metadataType}
    metafields
    channel {{slug}}
    billingAddress {addressType}
    note
    discount {moneyType}
    discountName
    voucherCode
    email
    quantity
    lines {checkoutLineType}
    subtotalPrice {taxedMoneyType}
    totalPrice {taxedMoneyType}
    token
    languageCode
    availablePaymentGateways {{
        id
        name
        config {{
            field
            value
        }}
    }}
}}"""
checkoutResultsType = f"""{{
    checkout {{
        id
        token
        totalPrice {taxedMoneyType}
        availablePaymentGateways {{
            id
            name
            config {{
                field
                value
            }}
        }}
    }}
    errors {checkoutErrorType}
}}"""
invoiceType = """{
    id
    createdAt
    updatedAt
    number
    externalUrl
    url
    status
    message
}"""
orderType = f"""{{
    id
    created
    updatedAt
    token
    status
    number
    isPaid
    billingAddress {addressType}
    paymentStatusDisplay
    statusDisplay
    metadata {metadataType}
    invoices {invoiceType}
    total {taxedMoneyType}
    lines {checkoutLineType}
}}"""
orderRestrictedType = f"""{{
    id
    created
    updatedAt
    token
    status
    number
    isPaid
    billingAddress {addressType}
    paymentStatusDisplay
    statusDisplay
    metadata {metadataType}
    total {taxedMoneyType}
    lines {checkoutLineType}
}}"""
# New preview feature in 3.5
validationRules = '''{
    checkRequiredFields: false
    checkFieldsFormat: true
    enableFieldsNormalization: true
}'''


class APIError(Exception):
    def __init__(self, message, errors: List[Dict[str, str]], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.errors = errors
        self.message = message

    @property
    def as_dict(self):
        _all = [self.message] + [_['message'] for _ in self.errors if not _['field']]
        return {'__all__': _all, **{_['field']: _['message'] for _ in self.errors if _['field']}}

    def __str__(self):
        return f'{self.message}: {self.errors}'


def get_product_variant(channel, sku):
    """Fetch a product variant by its SKU."""
    query = gql(
        f"""{{
        productVariant(channel: "{channel}", sku: "{sku}") {{
          {productVariantType}
          product {{ {productType} }}
        }}
    }}"""
    )
    results = client.execute(query)
    return results.get('productVariant')


def get_order_by_token(token):
    """Fetch an order by its token."""
    if not token:
        return None
    query = gql(
        f"""{{
        orderByToken(token: "{token}")
        {orderRestrictedType}
    }}"""
    )
    results = client.execute(query)
    return results['orderByToken']


def get_order(order_id):
    """Fetch an order by its ID."""
    if not order_id:
        return None
    query = gql(
        f"""{{
        order(id: "{order_id}")
        {orderType}
    }}"""
    )
    results = manage_client.execute(query)
    return results['order']


def get_orders(ids=None, after_cursor=None, **filters):
    """Fetch orders, filter with given filters. Requires a MANAGE_ORDERS token."""
    query_filters = {}
    if ids:
        query_filters['ids'] = '[{}]'.format(','.join(f'"{_}"' for _ in ids))
    query_filters = ' '.join(f'{k}: {v}' for k, v in query_filters.items())
    query = gql(
        f"""{{
        orders(
            sortBy: {{ direction: DESC field: CREATED_AT }}
            filter: {{ {query_filters} }}
            first: 100
            {after_cursor and "after:" + '"' + after_cursor + '"' or ""}
        ) {{
            pageInfo {{ hasNextPage hasPreviousPage startCursor endCursor }}
            edges {{
                node {orderType}
                cursor
            }}
            totalCount
        }}
    }}"""
    )
    results = manage_client.execute(query)
    return results['orders']


def orders_as_list(*args, **kwargs):
    """Fetch all orders with given filters, make multiple requests if paginated."""
    results = get_orders(*args, **kwargs)
    total_count = results['totalCount']
    data = [_['node'] for _ in results['edges']]
    while results['pageInfo']['hasNextPage']:
        kwargs['after_cursor'] = results['pageInfo']['endCursor']
        results = get_orders(*args, **kwargs)
        data.extend([_['node'] for _ in results['edges']])
    assert total_count == len(data)
    return data


def checkout_email_update(checkout_id: str, email: str):
    """Update checkout email."""
    mutation = gql(
        f"""mutation {{
        checkoutEmailUpdate(
            email: "{email}"
            id: "{checkout_id}"
        ) {checkoutResultsType}
      }}"""
    )
    results = client.execute(mutation)
    checkout = results['checkoutEmailUpdate']['checkout']
    errors = results['checkoutEmailUpdate']['errors']
    if errors:
        raise APIError('Unable to update checkout email', errors)
    return checkout


def checkout_create(channel: str, variant_id: str, quantity: int):
    """Create a checkout from given variant ID, quantity and request."""
    mutation = gql(
        f"""mutation {{
        checkoutCreate(
          input: {{
            channel: "{channel}",
            lines: [{{ quantity: {quantity}, variantId: "{variant_id}" }}]
            validationRules: {{
                shippingAddress: {validationRules}
                billingAddress: {validationRules}
            }}
          }}
        ) {checkoutResultsType}
      }}"""
    )
    results = client.execute(mutation)
    checkout = results['checkoutCreate']['checkout']
    errors = results['checkoutCreate']['errors']
    if errors:
        raise APIError('Unable to create checkout', errors)
    return checkout


def checkout_query(token):
    """Fetch a checkout with the given token."""
    query = gql(
        f"""{{
        checkout(token: "{token}")
        {checkoutType}
    }}"""
    )
    results = client.execute(query)
    checkout = results['checkout']
    return checkout


def checkout_payment_create(token, gateway, nonce, **metadata):
    # Passing anything other than "pending" to the dummy payment gateway
    # causes payment to be "captured" immediately.
    # dummy payment must never be charged or captured immediately!
    nonce = gateway == 'bank' and 'pending' or nonce

    gateway_id = GATEWAYS[gateway]
    metadata_input = None
    if any(metadata.values()):
        metadata_kv = [{'key': k, 'value': v} for k, v in metadata.items()]
        metadata_input = json.dumps(metadata_kv).replace('"key"', 'key').replace('"value"', 'value')
    mutation = gql(
        f"""mutation {{
      checkoutPaymentCreate(
        token: "{token}"
        input: {{
          gateway: "{gateway_id}"
          token: "{nonce}"
          storePaymentMethod: ON_SESSION
          {metadata_input and 'metadata: ' + metadata_input or ''}
        }}
      ) {{
        payment {{
          id
          chargeStatus
        }}
        errors {errorType}
      }}
    }}"""
    )
    results = client.execute(mutation)
    results = results['checkoutPaymentCreate']
    errors = results['errors']
    if errors:
        # Replace billingAddress field with __all__ to be able to convert to ValidationErrors
        for err in errors:
            if err.get('field') == 'billingAddress':
                err['field'] = ''
        raise APIError('Unable to create a payment for checkout', errors)
    payment = results['payment']
    return payment


def checkout_complete(token):
    mutation = gql(
        f"""mutation {{
      checkoutComplete(
        token: "{token}"
      ) {{
        order {orderRestrictedType}
        confirmationNeeded
        confirmationData
        errors {errorType}
      }}
    }}"""
    )
    results = client.execute(mutation)
    results = results['checkoutComplete']
    errors = results['errors']
    if errors:
        raise APIError('Unable to complete checkout', errors)
    order = results['order']
    return order


def update_metadata(token, **metadata):
    # N.B.: for Order and Checkout types it is recommended
    # to use token as the id input value, for other types use GraphQL ID.
    # Changing the Order metadata is possible only with the use of token.
    metadata_kv = [{'key': k, 'value': v} for k, v in metadata.items()]
    input = json.dumps(metadata_kv).replace('"key"', 'key').replace('"value"', 'value')
    mutation = gql(
        f"""mutation {{
      updateMetadata(
          id: "{token}"
          input: {input}
      ) {{
        metadataErrors {errorType}
        item {{
          metadata {metadataType}
        }}
        errors {errorType}
      }}
    }}"""
    )
    results = client.execute(mutation)['updateMetadata']
    errors = results['errors']
    if errors:
        raise APIError('Unable to update metadata', errors)
    metadata = results['item']['metadata']
    return metadata


def request_invoice(order_id: str):
    """Request an invoice from Saleor.

    Requires a token with MANAGE_ORDERS permission.
    """
    mutation = gql(
        f"""mutation {{
        invoiceRequest(
            orderId: "{order_id}"
        ) {{
            order {orderType}
            errors {errorType}
            invoice {invoiceType}
        }}
    }}"""
    )
    results = manage_client.execute(mutation)
    errors = results['invoiceRequest']['errors']
    if errors:
        raise APIError(f'Unable to request invoice for order ID={order_id}', errors)
    return results['invoiceRequest']['invoice']
