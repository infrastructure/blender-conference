from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.flatpages import views
from django.urls import include, path
from django.views.generic.base import RedirectView


def flatpage(request, edition_path, url):
    return views.flatpage(request, edition_path + '/' + url)


urlpatterns = [
    path('', include('conference_main.urls')),
    path('', include('tickets.urls', namespace='tickets')),
    # TODO: Make redirects dynamic based on current Edition
    path(
        'impressions',
        RedirectView.as_view(url='2024/photos/impressions/upload/', permanent=False),
        name='impressions',
    ),
    path(
        'festival',
        RedirectView.as_view(url='2024/festival/finals/', permanent=False),
        name='festival',
    ),
]

# Loginas and admin
urlpatterns += [path('admin/', include('loginas.urls')), path('admin/', admin.site.urls)]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Flatpages
urlpatterns += [
    path('code-of-conduct/', views.flatpage, {'url': '/code-of-conduct/'}, name='code-of-conduct'),
    path(
        'hosting-official-bcon/',
        views.flatpage,
        {'url': '/hosting-official-bcon/'},
        name='hosting-official-bcon',
    ),
    path('<str:edition_path>/', flatpage, {'url': ''}, name='homepage'),
    path(
        '<str:edition_path>/call-for-participation/',
        flatpage,
        {'url': 'call-for-participation/'},
        name='call_for_participation',
    ),
    path(
        '<str:edition_path>/code-of-conduct/',
        flatpage,
        {'url': 'code-of-conduct/'},
        name='code_of_conduct',
    ),
    path(
        '<str:edition_path>/speaker-checklist/',
        flatpage,
        {'url': 'speaker-checklist/'},
        name='speaker_checklist',
    ),
    path('<str:edition_path>/festival/', flatpage, {'url': 'festival/'}, name='festival'),
    path('<str:edition_path>/location/', flatpage, {'url': 'location/'}, name='location'),
    path(
        '<str:edition_path>/call-for-sponsorship/',
        flatpage,
        {'url': 'call-for-sponsorship/'},
        name='call_for_sponsorship',
    ),
]
