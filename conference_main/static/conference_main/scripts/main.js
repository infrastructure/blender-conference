function favoriteStarToggle(e) {
	e.preventDefault();
	let element = e.target;
	const csrftoken = getCookie('csrftoken');
	const field = element.dataset.field || 'favorite';
	$.post({
		url: element.dataset.toggleUrl,
		data: {
			[field]: !element.hasAttribute("data-is-checked")
		},
		dataType: 'json',
		headers: { 'X-CSRFToken': csrftoken },
		mode: 'same-origin', /* Do not send CSRF token to another domain. */
		success: function (data) {
			if (data[field]) {
				element.setAttribute("data-is-checked", "");
			} else {
				element.removeAttribute("data-is-checked");
			}
		}
	});
}

function getCookie(name) {
	let cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		const cookies = document.cookie.split(';');
		for (let i = 0; i < cookies.length; i++) {
			const cookie = cookies[i].trim();
			/* Does this cookie string begin with the name we want? */
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

function findParentWithAttribute(element, attribute) {
    // Traverse up the DOM tree
    while (element) {
        if (element.hasAttribute(attribute)) {
            return element;  // Found the parent with 'data-vote-url'
        }
        element = element.parentElement;  // Move to the parent
    }
    return null;  // If no element with 'data-vote-url' is found
}

function ratingStarToggle(e) {
	const csrftoken = getCookie('csrftoken');
	e.preventDefault();
	let element = findParentWithAttribute(e.target, 'data-rating');
	let voteStarsContainer = findParentWithAttribute(element, 'data-vote-url');
	if (!voteStarsContainer) {
		console.error('Could not find data-vote-url');
		return;
	}
	$.post({
		url: voteStarsContainer.dataset.voteUrl,
		data: {
			'rating': element.dataset.rating
		},
		dataType: 'json',
		headers: { 'X-CSRFToken': csrftoken },
		mode: 'same-origin', /* Do not send CSRF token to another domain. */
		success: function (data) {
			let updateRoots;
			if (voteStarsContainer.hasAttribute("data-sync-vote-stars-with-same-entry-id")) {
				updateRoots = $.find(".vote-stars[data-entry-id=" + voteStarsContainer.dataset.entryId + "]")
			} else {
				updateRoots = $(voteStarsContainer);
			}

			$(updateRoots).each(function (i, element) {
				$(element).find(".vote-star").each(
					function (i, element) {
						if (data.rating >= this.dataset.rating) {
							this.setAttribute("data-is-filled", "")
						} else {
							this.removeAttribute("data-is-filled")
						}
					}
				);
			});
		}
	});
}

function scheduleFilterUpdateCellsAndHeaders() {
	$(".event-cell").each(function (i, ec) {
		let eventCell = $(ec);
		if ($(eventCell).find(".event:not([data-filtered])").length) {
			eventCell.attr("data-no-events", null)
		} else {
			eventCell.attr("data-no-events", "data-no-events")
		}
	});

	$(".time-headers-lg").each(function (i, th) {
		let timeHeader = $(th);
		let allEventCells = timeHeader.nextUntil(".time-header-lg").filter(".event-cell");
		if (allEventCells.filter("[data-no-events]").length === 3) {
			timeHeader.attr("data-completely-no-events", "data-completely-no-events");
			allEventCells.attr("data-completely-no-events", "data-completely-no-events");
		} else {
			timeHeader.attr("data-completely-no-events", null);
			allEventCells.attr("data-completely-no-events", null);
		}
	});
}

function getFilterState(filterStates, filterName) {
	const elPath = ".schedule-filter[data-schedule-filter-type=" + filterName + "]";
	$(elPath).each(function (i, filter) {
		filterStates[filterName][filter.dataset.scheduleFilterValue] = filter.getElementsByTagName("input")[0].checked;
	});
}

function setFilterValue(filterStates, filterName) {
	if (!filterStates[filterName]) return;

	const elPath = ".schedule-filter[data-schedule-filter-type=" + filterName + "]";
	$(elPath).each(function (i, filter) {
		filter.getElementsByTagName("input")[0].checked = filterStates[filterName][filter.dataset.scheduleFilterValue];
	});
}

function scheduleFilterSetCookie() {
	let filterStates = {
		"location": {},
		"category": {},
		"favorites": {},
		"going": {}
	};

	getFilterState(filterStates, "location");
	getFilterState(filterStates, "category");
	getFilterState(filterStates, "favorites");
	getFilterState(filterStates, "going");

	Cookies.set("schedule-filter-status", JSON.stringify(filterStates));
}

function scheduleFilterLoadFromCookie() {
	const scheduleCookie = Cookies.get("schedule-filter-status");

	if (!!scheduleCookie) {
		const filterStates = JSON.parse(scheduleCookie);
		setFilterValue(filterStates, "location");
		setFilterValue(filterStates, "category");
		setFilterValue(filterStates, "favorites");
		setFilterValue(filterStates, "going");
	}

	scheduleFilterUpdateFilterStatus();
}

function scheduleFilterUpdateFilterStatus(event) {
	$(".event").each(function (i, event) {
		if (event.dataset.scheduleLocation && $(".schedule-filter[data-schedule-filter-type=location][data-schedule-filter-value="
			+ event.dataset.scheduleLocation + "] > input:checkbox:not(:checked)").length ||
			event.dataset.scheduleCategory && $(".schedule-filter[data-schedule-filter-type=category][data-schedule-filter-value="
				+ event.dataset.scheduleCategory + "] > input:checkbox:not(:checked)").length) {
			$(event).attr("data-filtered", "data-filtered");
		} else {
			$(event).attr("data-filtered", null);
		}

		if (($(event).find(".favorite-star[data-is-checked]").length === 0 &&
			$(".schedule-filter[data-schedule-filter-type=favorites] input:checkbox:checked").length) ||
			($(event).find(".going-star[data-is-checked]").length === 0 &&
				$(".schedule-filter[data-schedule-filter-type=going] input:checkbox:checked").length)) {
			$(event).attr("data-filtered-personal", "data-filtered-personal");
		} else {
			$(event).attr("data-filtered-personal", null);
		}
	});

	scheduleFilterUpdateCellsAndHeaders();
}

function scheduleFilterToggle(e) {
	/* Prevent disabling the last filter, no point in seeing no events. */
	if (($(".schedule-filter[data-schedule-filter-type=category] > input:checkbox:checked").length < 1) ||
		($(".schedule-filter[data-schedule-filter-type=location] > input:checkbox:checked").length < 1)) {
		e.target.checked = true;
		return;
	}

	scheduleFilterUpdateFilterStatus();
	scheduleFilterSetCookie();
}

function scheduleFilterClear(e) {
	$('.event').attr("data-filtered", null);
	$('.schedule-filter[data-schedule-filter-type=location] > input:checkbox, .schedule-filter[data-schedule-filter-type=category] > input:checkbox')
		.prop("checked", true);

	scheduleFilterSetCookie();
}

//Navigation Mobile button toggle
document.querySelectorAll('.js-toggle-menu').forEach(i => {
	i.addEventListener('click', (e) => {
		document.querySelector(e.target.dataset.target).classList.toggle('show')
	})
})

window.addEventListener("scroll", () => {
	if (window.pageYOffset > 40) {
		document.body.classList.add("is-scrolled")
	} else {
		document.body.classList.remove("is-scrolled")
	}
});

function scheduleSetDayCookie(dayNumber) {
	Cookies.set("schedule-current-day", dayNumber);
}

function scheduleHorizontalScrollRestore(currentDay) {
	const dayNumber = currentDay.getAttribute('data-day-number');
	const storageKey = `horizontalScrollPosition_${dayNumber}`;
	const scrollPosition = localStorage.getItem(storageKey);
	const scrollableDiv = currentDay.firstElementChild;

	if (scrollPosition) {
		scrollableDiv.scrollLeft = parseInt(scrollPosition);
	}
}

function scheduleSetDay(dayNumber) {
	/* Hide all days, except the one with data-day-number matching dayNumber. */
	const allDays = document.getElementsByClassName('js-schedule-day');
	for (const day of allDays) {
		day.classList.add('is-hidden');

		if (day.getAttribute('data-day-number') == dayNumber) {
			day.classList.remove('is-hidden');
			scheduleHorizontalScrollRestore(day);
		}
	}

	scheduleSetDayCookie(dayNumber);
	scheduleScrollToTimeslot();
}

/* Style button for the active day. */
function scheduleSetDayButtonActive(dayNumber) {
	/* Remove active class from all buttons. */
	$('.js-toggle-day').removeClass('is-active');

	/* Mark button with the matching day-number as active. */
	$('.js-toggle-day').each(function () {
		if ($(this).data('day-number') == dayNumber) {
			$(this).addClass('is-active');
		}
	});
}

function scheduleDayLoadFromCookie() {
	const scheduleCookie = Cookies.get("schedule-current-day");

	if (!scheduleCookie){
		scheduleSetDay('1');
		scheduleSetDayButtonActive('1');
		return;
	}

	scheduleSetDay(scheduleCookie);
	scheduleSetDayButtonActive(scheduleCookie);
}

/* Round to nearest half-hour. */
function roundMinutesToNearestHalfHour(date) {
	const roundedDate = new Date(date);
	const minutes = roundedDate.getMinutes();

	if (minutes >= 0 && minutes <= 29) {
		roundedDate.setMinutes(0);
	} else if (minutes >= 30 && minutes <= 59) {
		roundedDate.setMinutes(30);
	}

	return roundedDate;
}

/* Highlight talks going on right now. */
function scheduleGetCurrentDateTime() {
	let date = new Date();

	/* Set for debugging purposes: */
	// date = new Date('October 26, 2023 16:20');

	/* Round time since talks usually start at :00 or :30. */
	let roundedDate = roundMinutesToNearestHalfHour(date);
	/* Hardcoded to Amsterdam time. */
	/* TODO: Make timezone customizable for events in other countries. */
	var options = { timeZone: 'Europe/Amsterdam', day: '2-digit', month: '2-digit', year: '2-digit', hour: 'numeric', minute: '2-digit' };
	var currentDate = roundedDate.toLocaleString('en-GB', options);

	return currentDate;
}


/* Find the previous sibling with a matching selector. */
var getPreviousSibling = function (elem, selector) {
	var sibling = elem.previousElementSibling;

	if (!selector) return sibling;

	while (sibling) {
		if (sibling.matches(selector)) return sibling;
		sibling = sibling.previousElementSibling;
	}
};

/* Scroll to the highlighted time slot. */
function scheduleScrollToTimeslot(direction) {
	const slots = document.querySelectorAll('.happening-now');
	let nearestTimeCell;
	let alignScrollBlock;

	if (typeof slots[0] === "undefined") { return; }

	nearestTimeCell = slots[0];

	if (direction == "horizontal") {
		alignScrollBlock = "nearest";
	} else if (direction == "vertical") {
		nearestTimeCell = getPreviousSibling(slots[0], '.js-time-line-break');
		alignScrollBlock = 'start';
	}

	nearestTimeCell.scrollIntoView({ block: alignScrollBlock, inline: 'start' });
}

/* Highlight the current time slot and ongoing events. */
function scheduleHighlightCurrent(direction) {
	const slots = document.querySelectorAll('.js-time-slot');
	const scheduleTime = scheduleGetCurrentDateTime();

	if (!slots) { return; }

	slots.forEach(slot => {
		const currentTime = slot.getAttribute('data-day-hour');

		if (scheduleTime == currentTime) {
			slot.classList.add('happening-now');
		}
	});

	return scheduleScrollToTimeslot(direction);
}

/* Day toggle buttons. */
function scheduleDayToggleButtons() {
	$('.js-toggle-day').on('click', function () {
		/* Style button. */
		scheduleSetDayButtonActive();
		$(this).addClass('is-active');

		/* Actually hide/show events for x day. */
		let dayNumber = $(this).data('day-number');
		scheduleSetDay(dayNumber);
	});
}
