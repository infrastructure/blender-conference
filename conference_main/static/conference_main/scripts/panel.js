function panelAjaxRefresh(element) {
	$.get({
		url: element.dataset.refreshUrl,
		success: function (data) {
			element.innerHTML = data
		},
		complete: function () {
			setTimeout(
				function () {panelAjaxRefresh(element)},
				1000 * parseFloat(element.dataset.refreshTimeout)
			)
		}
	})
}

$(document).ready(function () {
	$(".panel-ajax-refresh").each(function (i, element) {panelAjaxRefresh(element)});
});
