const formVoteFinalsEl = document.forms['formVoteFinals'];
const warnEl = document.getElementById('totalPointsWarn');
const totalPointsLeftEl = document.getElementById('totalPointsDisplay');
const maxPointsPerCategory = 5;
const totalVotePointsMessage = `You can cast ${maxPointsPerCategory} voting points per category`;
const totalVotePointsMinMessage = 'Please use all available voting points';

function getPointsInputs(category) {
  const fieldset = formVoteFinalsEl.elements[category];
  let inputs = Array();
  for(let i=0; i < fieldset.elements.length; i++) {
    const el = fieldset.elements[i];
    if (!el.name.startsWith('points_for_entry_id_')) continue;
    inputs.push(el);
  };
  return inputs;
}

function showWarn(message) {
  if (!warnEl) return;
  warnEl.innerText = message;
  warnEl.classList.remove('d-none');
}
function hideWarn() {
  if (!warnEl) return;
  warnEl.classList.add('d-none');
}
function getSum(category) {
  const pointInputs = getPointsInputs(category);
  let sum = 0;
  pointInputs.forEach(function(r) {const v = parseInt(r.value) || 0; sum = sum + v});
  return sum;
}
function updatePoints() {
  const fieldsets = formVoteFinalsEl.getElementsByTagName('fieldset');
  const maxAllCategories = fieldsets.length * maxPointsPerCategory;
  let spentPoints = 0;
  for(let i=0; i < fieldsets.length; i++) {
    const el = fieldsets[i];
    const pointsLeftEl = el.getElementsByClassName('pointsLeft')[0];
    const points = getSum(el.id);
    pointsLeftEl.innerText = Math.max(0, maxPointsPerCategory - points);
    spentPoints += points;
  };
  totalPointsLeftEl.innerText = Math.max(0, maxAllCategories - spentPoints);
}

function clearCustomValidity() {
  const inputs = formVoteFinalsEl.getElementsByTagName('input');
  for(let i=0; i < inputs.length; i++) {
    const input = inputs[i];
    input.setCustomValidity('');
    input.reportValidity();
  };
}

function validateFinalVotesForm(ev) {
  if (!formVoteFinalsEl) return;
  clearCustomValidity();
  const remainingPoints = parseInt(totalPointsLeftEl.innerText);
  const input = formVoteFinalsEl.elements.vote;
  if (remainingPoints !== 0) {
    input.setCustomValidity(totalVotePointsMinMessage);
    ev.preventDefault();
    input.reportValidity();
    showWarn(totalVotePointsMinMessage);
  } else {
    input.setCustomValidity('');
    hideWarn();
    input.reportValidity();
  }
}

if (formVoteFinalsEl) {
  formVoteFinalsEl.addEventListener('submit', validateFinalVotesForm);

  formVoteFinalsEl.addEventListener('input', function(ev) {
    updatePoints();

    const input = ev.target;
    clearCustomValidity();
    hideWarn();

    const total = getSum(input.dataset.category);
    // console.log('input', ev.target, total, maxPointsPerCategory);
    if (total > maxPointsPerCategory) {
      input.setCustomValidity(totalVotePointsMessage);
      showWarn(totalVotePointsMessage);
    }
    input.reportValidity();
  });

  updatePoints();

  const pointsChangeButtons = document.querySelectorAll('.js-points-change');

  pointsChangeButtons.forEach(btn => {
    btn.addEventListener('click', function handleClick(event) {
      let inputId = btn.dataset.inputid;
      let voteDirection = btn.dataset.direction;
      let inputField = document.getElementById(inputId);
      const total = getSum(inputField.dataset.category);

      if (voteDirection == 'up'){

        if (total >= maxPointsPerCategory) {
          // Don't have to mark form invalid because input wasn't actually changed yet
          // inputField.setCustomValidity(totalVotePointsMessage);
          showWarn(totalVotePointsMessage);

          // inputField.reportValidity();
          return;
        }

        if (inputField.value < inputField.max) {
          inputField.value++;
          inputField.dispatchEvent(new Event('input', { bubbles: true }));
        }
      } else {
        if (inputField.value > inputField.min) {
          inputField.value--;
          inputField.dispatchEvent(new Event('input', { bubbles: true }));
        }
      }

      let entryTotalPoints = document.getElementById('total-points-' + inputId);
      entryTotalPoints.innerHTML = inputField.value;

      updatePoints();
    });
  });
};
