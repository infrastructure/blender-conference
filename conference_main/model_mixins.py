"""Mixins for sharing functionality between models."""
from typing import Set, Tuple, Mapping, Any
import logging

from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.db import models
from django.shortcuts import reverse

from conference_main.util import urlencode


logger = logging.getLogger(__name__)
OldStateType = Mapping[str, Any]
"""Type declaration for the old state of a model instance.

See RecordModificationMixin.pre_save_record().
"""


def _get_object_state(obj: object, fields=None, include_pk=False) -> dict:
    data = serializers.serialize('python', [obj], fields=fields)[0]
    if include_pk:
        data['fields']['pk'] = data['pk']
    return data['fields']


class RecordModificationMixin(models.Model):
    """Modification tracking for Django models.

    Tracks which fields have changed in the save() function, so that
    the model can send signals upon changes in fields.

    Only acts on fields listed in self.record_modification_fields.
    """

    class Meta:
        abstract = True

    record_modification_fields: Set[str]

    def _was_modified(self, old_instance: object, update_fields=None) -> bool:
        """Return True if the record is modified.

        Only checks fields listed in self.record_modification_fields.
        """
        for field in self.record_modification_fields:
            # If update_fields was given and this field was NOT in it,
            # its value definitely won't be changed:
            if update_fields is not None and field not in update_fields:
                continue
            old_val = getattr(old_instance, field, ...)
            new_val = getattr(self, field, ...)
            if old_val != new_val:
                return True
        return False

    def pre_save_record(self, *args, **kwargs) -> Tuple[bool, OldStateType]:
        """Return previous state of this object.

        Only records fields listed in self.record_modification_fields.

        **Important!**: pre_save_record must receive `update_fields` via kwargs,
        otherwise it will report perceived changes that don't actually reflect
        changes to records stored in the database.

        :returns: (was changed, old state) tuple.
        """
        if not self.pk:
            return True, {}

        try:
            db_instance = type(self).objects.get(id=self.pk)
        except type(self).DoesNotExist:
            return True, {}

        update_fields = kwargs.get('update_fields')
        was_modified = self._was_modified(db_instance, update_fields=update_fields)
        old_instance_data = _get_object_state(db_instance, fields=self.record_modification_fields)
        return was_modified, old_instance_data


class CreatedUpdatedMixin(models.Model):
    """Store creation and update timestamps."""

    class Meta:
        abstract = True

    created_at = models.DateTimeField('date created', auto_now_add=True)
    updated_at = models.DateTimeField('date edited', auto_now=True)

    def get_logentries_url(self) -> str:
        content_type_id = ContentType.objects.get_for_model(self.__class__).pk
        object_id = self.pk
        app_label = 'admin'
        model = 'logentry'
        path = reverse(f'admin:{app_label}_{model}_changelist')
        query = urlencode(
            {
                'content_type__id__exact': content_type_id,
                'object_id__exact': object_id,
            }
        )
        return f'{path}?{query}'
