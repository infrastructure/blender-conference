from django import template
import urllib.parse

register = template.Library()


@register.filter
def website_hostname(url):
    """Given a website url, return the hostname only.

    This useful for showing a url in the frontend. For example:
    https://blender.org -> blender.org
    """

    url = urllib.parse.urlparse(url)

    if not url.hostname:
        return url.path

    return url.hostname.replace("www.", "")


@register.filter
def replace(value, arg):
    """
    Replacing filter
    Use `{{ "aaa"|replace:"a|b" }}`
    """
    if len(arg.split('|')) != 2:
        return value

    what, to = arg.split('|')
    return value.replace(what, to)
