import datetime

from django import template

from conference_main.util import login_url_with_redirect
from conference_main.util import compact_timesince

register = template.Library()

register.filter('login_url_with_redirect', login_url_with_redirect)
register.filter('compact_timesince', compact_timesince)


@register.filter('from_timestamp')
def from_timestamp(timestamp) -> datetime.datetime:
    """Convert a given timestamp seconds to a datetime object."""
    if not timestamp:
        return ''
    return datetime.date.fromtimestamp(int(timestamp))


@register.filter(name='subtract')
def _subtract(value0, value1) -> int:
    return int(value0) - int(value1)
