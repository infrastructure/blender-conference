from typing import Optional, Dict

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Div
from django import forms

from conference_main.models import Event, Location, FestivalEntry, Profile
from conference_main.widgets import PortraitImageWidget


def _get_festival_entry_layout():
    return Layout(
        Div(
            Div('title', css_class='col'),
            Div('video_link', css_class='col'),
            css_class='row',
        ),
        Fieldset(
            '',
            'description',
            'credits',
            'website',
            'thumbnail',
        ),
    )


class PresentationForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'description', 'proposal']
        labels = {'name': 'Presentation Title', 'description': 'Abstract'}

        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'The 5 things you did not know...'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['description'].required = True


class MessageForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea)


class FestivalEntryForm(forms.ModelForm):
    class Meta:
        model = FestivalEntry
        fields = [
            'title',
            'description',
            'credits',
            'website',
            'video_link',
            'thumbnail',
        ]
        labels = {
            'name': 'Title',
            'description': 'Description',
            'credits': 'Credits',
            'website': 'Website',
            'video_link': 'Video Link',
            'thumbnail': 'Thumbnail',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = _get_festival_entry_layout()

    def clean(self) -> Dict[str, object]:
        cleaned_data = super().clean()

        video_link: Optional[str] = cleaned_data.get('video_link')

        self.instance: FestivalEntry
        if (
            self.instance.status in ('accepted', 'winner')
            and self.instance.video_link != video_link
        ):
            raise forms.ValidationError(
                'You cannot change the video link after an entry has been accepted.'
            )

        return cleaned_data


class FestivalEntryStaffForm(forms.ModelForm):
    class Meta:
        model = FestivalEntry
        fields = [
            'title',
            'description',
            'credits',
            'website',
            'video_link',
            'status',
            'category',
            'thumbnail',
        ]
        labels = {
            'name': 'Title',
            'description': 'Description',
            'credits': 'Credits',
            'website': 'Website',
            'video_link': 'Video Link',
            'status': 'Status',
            'category': 'Category',
            'thumbnail': 'Thumbnail',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = _get_festival_entry_layout()
        self.helper.layout.append(
            Div(
                Div('category', css_class='col'),
                Div('status', css_class='col'),
                css_class='row',
            )
        )

    def clean(self) -> Dict[str, object]:
        cleaned_data = super().clean()

        status: Optional[str] = cleaned_data.get('status')
        category: Optional[str] = cleaned_data.get('category')

        if status in ('nominated', 'winner') and not category:
            raise forms.ValidationError(
                'If the entry is nominated or has won a category should be set as well.'
            )

        return cleaned_data


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('full_name', 'title', 'company', 'country', 'bio', 'url', 'portrait', 'is_public')
        widgets = {'portrait': PortraitImageWidget}


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = '__all__'
        widgets = {
            'color': forms.TextInput(attrs={'type': 'color'}),
        }
