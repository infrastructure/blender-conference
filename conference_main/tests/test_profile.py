from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase
from django.urls import reverse

from conference_main.models import Profile, Edition, SiteSettings, Event
from conference_main.util import login_url_with_redirect


class ProfileModelTest(TestCase):
    def setUp(self) -> None:
        self.owner_user = User.objects.create_user(
            username='owner_user', email='test@test.nl', password='12345'
        )
        self.unrelated_user = User.objects.create_user(username='unrelated_user', password='12345')
        self.staff_user = User.objects.create_user(
            username='staff_user', password='12345', is_superuser=True, is_staff=True
        )

        self.edition = Edition.objects.create(
            year=2019, location='Nowhere', path='2019', title='Blender Conference 2019'
        )
        self.site_settings = SiteSettings.objects.create(
            site=Site.objects.get_current(), current_edition=self.edition
        )

    def test_profile_created_after_user(self):
        """Ensure that profile is attached to a newly created user."""
        user = User.objects.create_user(username='test_user', password='12345')
        self.assertIsInstance(user.profile, Profile)

    def test_profile_update_get(self):
        url = reverse('profile_update')

        # Test a user can view the update form.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        # Test an anonymous user cannot view the submission form.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_profile_update_post(self):
        full_name = 'No Body амет'
        company = 'Hello Inc. амет'
        title = 'CEO'
        bio = 'I am nobody амет'
        profile_url = 'http://www.google.nl'

        url = reverse('profile_update')
        data = {
            'full_name': full_name,
            'company': company,
            'bio': bio,
            'country': 'NL',
            'url': profile_url,
            'title': title,
        }

        # Test a user can update his profile.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(url, response.url)

        profile = self.owner_user.profile
        profile.refresh_from_db()
        self.assertEqual(full_name, profile.full_name)
        self.assertEqual(company, profile.company)
        self.assertEqual(title, profile.title)
        self.assertEqual(bio, profile.bio)
        self.assertEqual(profile_url, profile.url)
        self.assertEqual('Netherlands', profile.country.name)

        # Test an anonymous user cannot update his profile.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_profile_partial_update_post(self):
        full_name = 'No Body амет'
        url = reverse('profile_update')
        data = {'full_name': full_name}
        # Test a user can update his profile.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(url, response.url)

    def test_profile_detail(self):
        event = Event.objects.create(
            user=self.owner_user,
            name='How to train your dragon вениам сенсерит',
            description='Лорем ипсум долор сит амет, ех хас синт вениам сенсерит, '
            'дуо ут юсто игнота аппетере. Ад пхаедрум омиттантур при, '
            'ид вел аццусамус маиестатис адверсариум.',
            edition=self.edition,
            status='submitted',
        )
        event.speakers.add(self.owner_user.profile)

        url = reverse('profile_detail', kwargs={'pk': self.owner_user.profile.pk})

        # Test the owner and the staff can see the profile.
        for user in (self.owner_user, self.staff_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an unrelated user cannot see the profile.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Test an anonymous user cannot see the profile.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        self.edition.speakers_viewable = True
        self.edition.save()
        event.status = 'accepted'
        event.save()

        # Test the owner and the staff and an unrelated user can see the profile
        # of an accepted speaker.
        for user in (self.owner_user, self.staff_user, self.unrelated_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an anonymous user can see the profile of an accepted speaker.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
