import datetime
import json
from typing import Optional

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core import mail
from django.test import TestCase
from django.urls import reverse

from conference_main.models import Edition, Event, Location, MessageExchange, SiteSettings, Day
from conference_main.util import login_url_with_redirect


class EventTest(TestCase):
    def setUp(self) -> None:
        self.owner_user = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )
        self.owner_user.profile.full_name = 'Owner User'
        self.owner_user.profile.save()

        self.unrelated_user = User.objects.create_user(username='unrelated_user', password='12345')
        self.staff_user = User.objects.create_user(
            username='staff_user', password='12345', is_superuser=True, is_staff=True
        )
        self.staff_user.profile.full_name = 'Mr. Staﬀ'
        self.staff_user.profile.save()

        self.edition = Edition.objects.create(
            year=2019, location='Nowhere', path='2019', title='Blender Conference 2019'
        )
        self.site_settings = SiteSettings.objects.create(
            site=Site.objects.get_current(), current_edition=self.edition
        )

    def _create_event(self) -> Event:
        return Event.objects.create(
            user=self.owner_user,
            name='How to train your dragon вениам сенсерит',
            proposal='Лорем ипсум долор сит амет, ех хас синт вениам сенсерит, '
            'дуо ут юсто игнота аппетере. Ад пхаедрум омиттантур при, '
            'ид вел аццусамус маиестатис адверсариум.',
            edition=self.edition,
        )

    def _create_location(self) -> Location:
        return Location.objects.create(
            name='theater',
            slug='theater',
        )

    def test_creation(self):
        event = self._create_event()

        self.assertEqual('submitted', event.status)

    def test_submit_get(self):
        url = reverse('presentation_submit', kwargs={'edition_path': self.edition.path})

        self.edition.is_archived = True
        self.edition.save()

        # Test a user cannot view the submission form if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.presentation_submissions_open = False
        self.edition.save()

        # Test a user can view the submission form even if the submissions are closed.
        # This is because from past experience we know that being able to send users
        # who were too late with submissions a link to the submission form is really handy.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        # Test an anonymous user cannot view the submission form.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_submit_post(self):
        name = 'How to train your dragon вениам сенсерит'
        proposal = 'Лорем ипсум долор сит амет, ех хас синт вениам сенсерит,'
        description = 'Like the proposal above but shorter'

        url = reverse('presentation_submit', kwargs={'edition_path': self.edition.path})
        data = {'name': name, 'description': description, 'proposal': proposal}

        self.edition.is_archived = True
        self.edition.save()

        # Test a user cannot submit presentations if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.presentation_submissions_open = False
        self.edition.save()

        # Test a user can submit a presentation even if the submissions are closed.
        # This is because from past experience we know that being able to send users
        # who were too late with submissions a link to the submission form is really handy.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('account_presentations_list'), response.url)

        event = Event.objects.get(user=self.owner_user)
        self.assertEqual(name, event.name)
        self.assertEqual(proposal, event.proposal)
        self.assertEqual(description, event.description)

        # Test an anonymous user cannot submit presentations.
        self.client.logout()
        response = self.client.post(url, data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_presentation_detail(self):
        event = self._create_event()
        url = reverse(
            'presentation_detail', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )

        # Test the owner and staff can view the event.
        for user in (self.owner_user, self.staff_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an unrelated user cannot view a non-accepted event.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Test an anonymous user cannot view a non-accepted event.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

        # Tests for events that have been accepted.
        for status in ('accepted', 'cancelled'):
            event.status = status
            event.save()

            # Test an unrelated user cannot view an accepted event.
            self.client.force_login(self.unrelated_user)
            response = self.client.get(url)
            self.assertEqual(403, response.status_code)

            # Test that an anonymous user gets redirected to login
            self.client.logout()
            response = self.client.get(url)
            self.assertEqual(302, response.status_code)

    def test_presentation_update_get(self):
        event = self._create_event()

        url = reverse(
            'presentation_update', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )

        self.edition.is_archived = True
        self.edition.save()

        # Test the owner cannot view the update form if `Edition.is_archived == True`
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Test the owner and staff can view the update form.
        for user in (self.owner_user, self.staff_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an unrelated user cannot view the update form.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Test an anonymous user cannot view the update form.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_presentation_update_post(self):
        event = self._create_event()

        name = 'How to train your dragon вениам сенсерит aaa'
        description = 'Лорем ипсум долор сит амет, ех хас синт вениам сенсерит, aaa'

        url = reverse(
            'presentation_update', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )
        data = {'name': name, 'description': description}

        review_url = reverse(
            'presentation_review', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )

        self.edition.is_archived = True
        self.edition.save()

        # Test that the owner cannot update an event if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Test that the owner can update an event.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        event.refresh_from_db()
        self.assertEqual(name, event.name)
        self.assertEqual(description, event.description)

        # Test that the staff can update an event.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={**data, 'status': 'submitted'})
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        event.refresh_from_db()
        self.assertEqual(name, event.name)
        self.assertEqual(description, event.description)

        # Test that unrelated users cannot update an event.
        self.client.force_login(self.unrelated_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        # Test that anonymous users cannot update an event.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

        # Test that owner cannot update the status of an event.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={**data, 'status': 'accepted'})
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        event.refresh_from_db()
        self.assertEqual('submitted', event.status)

        # Test that staff users can update the status of an event.
        self.client.force_login(self.staff_user)
        response = self.client.post(url, data={**data, 'status': 'accepted'})
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        event.refresh_from_db()
        self.assertEqual('accepted', event.status)

    def test_account_presentations_list(self):
        url = reverse('account_presentations_list')

        # Test that a user gets a message when he has no events.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'No proposal submitted.', response.content)

        # Test that a user can see his events.
        self.client.force_login(self.owner_user)
        self._create_event()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertNotIn(b'No proposal submitted.', response.content)

        # Test an anonymous user cannot see any events.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

    def test_account_favorites_list(self):
        url = reverse('account_favorites_list')

        # Test that a user gets a message when he has no favorites.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'Presentations you mark as favorite will show up here.', response.content)

        # Test that a user can see his favorite events.
        self.client.force_login(self.owner_user)
        event = self._create_event()
        event.favorites.add(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertNotIn(b'Presentations you mark as favorite will show up here.', response.content)

        # Test that an unrelated user cannot see favorite events of other people that are not yet
        # accepted.
        event.favorites.add(self.unrelated_user)
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'Presentations you mark as favorite will show up here.', response.content)

        # Test an unrelated user can see favorite events of other people that are accepted or
        # cancelled.
        for status in ('accepted', 'cancelled'):
            event.status = status
            event.save()
            self.client.force_login(self.unrelated_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)
            self.assertNotIn(
                b'Presentations you mark as favorite will show up here.', response.content
            )

        # Test an anonymous user cannot see any favorites.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

    def test_presentation_favorite(self):
        event = self._create_event()

        url = reverse(
            'presentation_favorite', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )

        # Test that a user can favorite an event.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={'favorite': 'true'})
        self.assertEqual(200, response.status_code)
        self.assertDictEqual({'favorite': True}, json.loads(response.content))

        # Test that a user can unfavorite an event.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={'favorite': 'false'})
        self.assertEqual(200, response.status_code)
        self.assertDictEqual({'favorite': False}, json.loads(response.content))

        # Test an anonymous user cannot see any favorites.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

    def test_message_presentation_form(self):
        event = self._create_event()

        message = 'זה כיף סתם לשמוע איך תנצח קרפד עץ טוב בגן.'

        url = reverse(
            'message_presentation_form', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )
        data = {'message': message}

        review_url = reverse(
            'presentation_review', kwargs={'edition_path': event.edition.path, 'pk': event.pk}
        )

        self.edition.is_archived = True
        self.edition.save()

        # Make sure the owner cannot post a message if `Edition.is_archived == True`.
        self.client.force_login(self.owner_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Make sure the owner can post a message.
        self.client.force_login(self.owner_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        message_exchange: Optional[MessageExchange] = MessageExchange.objects.filter(
            message__user=self.owner_user
        ).first()
        self.assertIsNotNone(message_exchange)
        assert message_exchange is not None
        self.assertEqual(event, message_exchange.content_object)
        self.assertEqual(message, message_exchange.message.content)

        # Make sure an email notification was sent.
        self.assertEqual(1, len(mail.outbox))
        self.assertIn(review_url, mail.outbox[0].body)

        # Make sure a staff user can post a message.
        self.client.force_login(self.staff_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(review_url, response.url)

        message_exchange = MessageExchange.objects.filter(message__user=self.staff_user).first()
        self.assertIsNotNone(message_exchange)
        assert message_exchange is not None
        self.assertEqual(event, message_exchange.content_object)
        self.assertEqual(message, message_exchange.message.content)

        # Make sure an email notification was sent.
        self.assertEqual(1, len(mail.outbox))
        self.assertIn(review_url, mail.outbox[0].body)

        # Make sure an unrelated user cannot post a message.
        self.client.force_login(self.unrelated_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        # Make sure an anonymous user cannot post a message.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_schedule(self):
        event = self._create_event()
        day = Day.objects.create(edition=self.edition, date=datetime.date(2019, 9, 27), number=1)
        event.day = day
        event.time = datetime.time(12, 00)
        event.save()

        url = reverse('schedule', kwargs={'edition_path': self.edition.path})

        self.edition.schedule_status = Edition.SCHEDULE_UNAVAILABLE
        self.edition.save()

        # Make sure an anonymous user cannot view an unavailable schedule.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        # Make sure a logged in user can view an unavailable schedule.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Make sure a staff user can view an unavailable schedule.
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        for schedule_status in (Edition.SCHEDULE_PROPOSED, Edition.SCHEDULE_FINAL):
            self.edition.schedule_status = schedule_status
            self.edition.save()

            # Make sure an anonymous user can view an available schedule.
            self.client.logout()
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

            # Make sure a logged in user can view an available schedule.
            self.client.force_login(self.owner_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

            # Make sure a staff user can view an available schedule.
            self.client.force_login(self.staff_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

    def test_presentations_json_list(self):
        event = self._create_event()
        event.location = self._create_location()
        event.category = 'panel'
        event.website = 'https://cönference.blender.org/'

        day = Day.objects.create(edition=self.edition, date=datetime.date(2019, 9, 27), number=1)
        event.day = day
        event.time = datetime.time(12, 00)
        event.save()

        event.speakers.add(self.owner_user.profile)
        event.speakers.add(self.staff_user.profile)

        url = reverse('presentation_list_json', kwargs={'edition_path': self.edition.path})

        # Make sure the schedule is empty if our one event isn't accepted.
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual({}, response.json(), 'Expected empty schedule')

        # Accept the event for the re
        event.status = 'accepted'
        event.save()

        expected_schedule = {
            str(event.id): {
                'title': 'How to train your dragon вениам сенсерит',
                'day': '2019-09-27',
                'start_time': '12:00',
                'duration_minutes': None,
                'speakers': 'Owner User, Mr. Staﬀ',
                'location': 'theater',
                'category': 'Panel',
                'website': 'https://cönference.blender.org/',
            }
        }
        self.maxDiff = 1024  # to be able to show differences in schedule

        # Just a sanity check on the as_json() function.
        self.assertEqual(expected_schedule[str(event.id)], event.as_json())

        # Check access to unavaiable schedule
        self.edition.schedule_status = Edition.SCHEDULE_UNAVAILABLE
        self.edition.save()

        # Make sure an anonymous user cannot view an unavailable schedule.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

        # Make sure a logged in user cannot view an unavailable schedule.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Make sure a staff user can view an unavailable schedule.
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        for schedule_status in (Edition.SCHEDULE_PROPOSED, Edition.SCHEDULE_FINAL):
            self.edition.schedule_status = schedule_status
            self.edition.save()

            # Make sure an anonymous user can view an available schedule.
            self.client.logout()
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

            # Make sure a logged in user can view an available schedule.
            self.client.force_login(self.owner_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

            # Make sure a staff user can view an available schedule.
            self.client.force_login(self.staff_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)
            self.assertEqual(expected_schedule, response.json())

    def test_speakers(self):
        event_1 = self._create_event()
        event_1.status = 'accepted'
        event_1.speakers.add(self.owner_user.profile)
        event_1.save()

        url = reverse('speakers', kwargs={'edition_path': self.edition.path})

        self.edition.speakers_viewable = True
        self.edition.save()

        # Make sure an anonymous user can view the speakers.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        event_2 = self._create_event()
        event_2.status = 'accepted'
        event_2.speakers.add(self.owner_user.profile)
        event_2.save()

        # Make sure the speakers are distinct.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
