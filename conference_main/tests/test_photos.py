from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test import TestCase

from conference_main.models import Photo, Edition, SiteSettings, Album
from tickets.tests.factories import TicketFactory


class PhotosModelTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username='owner_user', email='test@test.nl', password='12345'
        )
        self.superuser = User.objects.create_user(
            username='superuser', password='12345', is_superuser=True, is_staff=True
        )

        self.edition = Edition.objects.create(
            year=2019, location='Nowhere', path='2019', title='Blender Conference 2019'
        )
        self.site_settings = SiteSettings.objects.create(
            site=Site.objects.get_current(), current_edition=self.edition
        )
        self.album = Album.objects.create(order=1, edition=self.edition, slug='test-album')

        # Sanity check
        self.assertEqual(Photo.objects.count(), 0)
        self.assertEqual(self.album.photos.count(), 0)
        self.assertEqual(self.edition.photos.count(), 0)

    def test_upload_photo_anonymous_redirects_to_login(self):
        response = self.client.post(self.album.get_upload_url(), {})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/oauth/login?next=/2019/photos/test-album/upload/')

    def test_upload_photo_not_attendee_not_allowed(self):
        self.client.force_login(self.user)
        with open('assets_shared/assets/images/blender_made_by_you.jpg', 'rb') as f:
            response = self.client.post(self.album.get_upload_url(), {'file': f})

        self.assertEqual(response.status_code, 403)

    def test_upload_photo_superuser(self):
        self.client.force_login(self.superuser)
        with open('assets_shared/assets/images/blender_made_by_you.jpg', 'rb') as f:
            response = self.client.post(self.album.get_upload_url(), {'file': f})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Photo.objects.count(), 1)
        self.assertEqual(self.album.photos.count(), 1)
        self.assertEqual(self.edition.photos.count(), 1)
        photo = Photo.objects.first()
        self.assertEqual(photo.user, self.superuser)

    def test_upload_photo_attendee_is_upload_open_false_not_allowed(self):
        TicketFactory(
            user=self.user,
            quantity=1,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=True,
            edition=self.edition,
        )

        self.client.force_login(self.user)
        with open('assets_shared/assets/images/blender_made_by_you.jpg', 'rb') as f:
            response = self.client.post(self.album.get_upload_url(), {'file': f})

        self.assertEqual(response.status_code, 403)

    def test_upload_photo_attendee(self):
        TicketFactory(
            user=self.user,
            quantity=1,
            order_token='8ef27213-845a-4bb9-a917-d342b7566a1c',
            is_paid=True,
            edition=self.edition,
        ).process_new_ticket()
        self.album.is_upload_open = True
        self.album.save(update_fields={'is_upload_open'})

        self.client.force_login(self.user)
        with open('assets_shared/assets/images/blender_made_by_you.jpg', 'rb') as f:
            response = self.client.post(self.album.get_upload_url(), {'file': f})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Photo.objects.count(), 1)
        self.assertEqual(self.album.photos.count(), 1)
        self.assertEqual(self.edition.photos.count(), 1)
        photo = Photo.objects.first()
        self.assertEqual(photo.user, self.user)
