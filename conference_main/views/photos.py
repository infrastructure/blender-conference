import datetime
from typing import Optional, Dict

from django import urls
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.db.models import QuerySet
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseBase,
    JsonResponse,
)
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import ListView
from django.views.generic.base import View, TemplateResponseMixin

from conference_main import permissions, models


class AlbumSlideshowView(TemplateResponseMixin, View):
    photo_duration: datetime.timedelta = datetime.timedelta(seconds=5)
    slideshow_history: int = 10
    template_name = 'conference_main/photos/slideshow.pug'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: models.Edition = get_object_or_404(
            models.Edition, path=kwargs['edition_path']
        )
        self.album: models.Album = get_object_or_404(
            models.Album, edition=self.edition, slug=kwargs['album']
        )

    def get_current_photo(self) -> Optional[models.Photo]:
        latest_photos = self.album.photos.order_by('-created_at')[: self.slideshow_history]
        if latest_photos:
            last_photo = latest_photos[0]
            last_photo_time: datetime.datetime = last_photo.created_at
            time_since_last_photo = timezone.now() - last_photo_time
            photos_since_last_photo = int(
                time_since_last_photo.total_seconds() / self.photo_duration.total_seconds()
            )
            current_photo = photos_since_last_photo % min(
                self.slideshow_history, len(latest_photos)
            )
            return latest_photos[current_photo]
        else:
            return None

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponseBase:
        photo = self.get_current_photo()

        if 'response_type' in self.request.GET and self.request.GET['response_type'] == 'json':
            if not photo:
                return JsonResponse()

            return JsonResponse({
                'photo': photo.file.url,
                'authorFullName': photo.user.profile.full_name or photo.user.username,
                'authorTitle': photo.user.profile.title,
                'authorCompany': photo.user.profile.company,
                })
        else:
            return self.render_to_response(
                {
                    'album': self.album,
                    'photo': photo,
                    'absolute_album_url': request.build_absolute_uri(
                        urls.reverse(
                            'album',
                            kwargs={'edition_path': self.edition.path, 'album': self.album.slug},
                        )
                    ),
                }
            )


class AlbumUploadView(LoginRequiredMixin, PermissionRequiredMixin, TemplateResponseMixin, View):
    template_name = 'conference_main/photos/upload.pug'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: models.Edition = get_object_or_404(
            models.Edition, path=kwargs['edition_path']
        )
        self.album: models.Album = get_object_or_404(
            models.Album, edition=self.edition, slug=kwargs['album']
        )

    def has_permission(self) -> bool:
        return permissions.can_upload_photos(self.album, self.request.user)

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        return self.render_to_response({'album': self.album})

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        if not request.FILES:
            return HttpResponseBadRequest('No file provided.')
        else:
            for file in request.FILES.values():
                try:
                    photo: models.Photo = models.Photo.objects.create(
                        edition=self.edition, file=file, user=request.user
                    )
                    photo.albums.add(self.album)
                except ValidationError as e:
                    return HttpResponseBadRequest(e)
            return HttpResponse('Photo was uploaded successfully')


class AlbumView(ListView):
    model = models.Photo
    template_name = 'conference_main/photos/album.pug'
    ordering = ('-created_at',)
    paginate_by = 24
    context_object_name = 'photos'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: models.Edition = get_object_or_404(
            models.Edition, path=kwargs['edition_path']
        )
        self.album: models.Album = get_object_or_404(
            models.Album, edition=self.edition, slug=kwargs['album']
        )

    def get_context_data(self, **kwargs: object) -> Dict[str, object]:
        return super().get_context_data(
            **{
                **kwargs,
                **{
                    'album': self.album,
                    'can_upload_photos': permissions.can_upload_photos(
                        self.album, self.request.user
                    ),
                },
            }
        )

    def get_queryset(self) -> 'QuerySet[models.Photo]':
        return self.album.photos.all()


class AlbumsView(ListView):
    model = models.Album
    template_name = 'conference_main/photos/albums.pug'
    ordering = ('order',)
    context_object_name = 'albums'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: models.Edition = get_object_or_404(
            models.Edition, path=kwargs['edition_path']
        )
        self.object_list = self.get_queryset()

    def get_queryset(self) -> 'QuerySet[models.Album]':
        return self.edition.albums.all()

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        if len(self.object_list) == 1:
            album: models.Album = self.object_list[0]
            return HttpResponseRedirect(
                urls.reverse(
                    'album', kwargs={'edition_path': self.edition.path, 'album': album.slug}
                )
            )
        else:
            return super().get(request, *args, **kwargs)
