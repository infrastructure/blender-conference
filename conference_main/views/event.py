import itertools
import random

from typing import Dict, Sequence, Type, Iterable, cast, List
from typing import Optional

from django import urls
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import QuerySet
from django.db.models.base import Model
from django.http import HttpResponseRedirect, JsonResponse
from django.http.request import HttpRequest
from django.views.generic import FormView, ListView, TemplateView
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from conference_main import permissions
from conference_main.forms import PresentationForm, MessageForm
from conference_main.models import Edition, Event, Message, MessageExchange, Profile
from conference_main.views import SingleObjectStoreMixin
import tickets.queries


class PresentationSubmitView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    """Form view for the submission of a presentation."""

    template_name = 'conference_main/presentations/submit.pug'
    form_class = PresentationForm

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def has_permission(self) -> bool:
        return permissions.can_add_event(self.edition, self.request.user)

    def get_success_url(self):
        return urls.reverse('account_presentations_list')

    def form_valid(self, form):
        event = Event.objects.create(
            user=self.request.user,
            edition=self.edition,
            **form.cleaned_data,
        )
        # Add the submitter profile as speaker
        event.speakers.add(self.request.user.profile)

        return super().form_valid(form)


class AccountPresentationsListView(LoginRequiredMixin, ListView):
    template_name = 'conference_main/presentations/account_list.pug'

    def get_queryset(self):
        return Event.objects.filter(user=self.request.user).order_by(
            '-edition__year', '-created_at'
        )


class AccountFavoritesListView(LoginRequiredMixin, ListView):
    template_name = 'conference_main/presentations/favorites.pug'
    context_object_name = 'events'

    def get_queryset(self):
        my_events = Event.objects.filter(favorites__pk=self.request.user.pk, user=self.request.user)
        other_events = Event.objects.filter(
            favorites__pk=self.request.user.pk, status__in=('accepted', 'cancelled')
        )
        return (my_events | other_events).order_by('-edition__year', '-created_at')


class PresentationFavoriteAjaxView(LoginRequiredMixin, SingleObjectStoreMixin[Event], View):
    model = Event

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        favorite: Optional[str] = self.request.POST.get('favorite')

        if favorite == 'true':
            self.object.favorites.add(self.request.user)
            return JsonResponse({'favorite': True})
        elif favorite == 'false':
            self.object.favorites.remove(self.request.user)
            return JsonResponse({'favorite': False})
        else:
            return JsonResponse(
                {'error': 'Field "favorite" should be either "true" or "false".'}, status=400
            )


class PresentationGoingAjaxView(LoginRequiredMixin, SingleObjectStoreMixin[Event], View):
    model = Event

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        if not tickets.queries.is_attending_edition(request.user, self.object.edition):
            return JsonResponse({'error': 'For attendees only'}, status=400)

        going: Optional[str] = self.request.POST.get('going')

        if going == 'true':
            self.object.attendees.add(self.request.user)
            return JsonResponse({'going': True})
        elif going == 'false':
            self.object.attendees.remove(self.request.user)
            return JsonResponse({'going': False})
        else:
            return JsonResponse(
                {'error': 'Field "going" should be either "true" or "false".'}, status=400
            )


class PresentationDetailView(SingleObjectStoreMixin[Event], PermissionRequiredMixin, TemplateView):
    model = Event
    template_name = 'conference_main/presentations/detail.pug'

    def has_permission(self) -> bool:
        return permissions.can_view_event(self.object, self.request.user)

    def get_context_data(self, **kwargs):
        extra_context = {
            'permissions': {
                'can_change': permissions.can_change_event(self.object, self.request.user),
                'can_view_messages': permissions.can_view_messages_on_event(
                    self.object, self.request.user
                ),
                'can_add_message': permissions.can_add_message_to_event(
                    self.object, self.request.user
                ),
            },
            'speakers': [],
        }

        for speaker in self.object.speakers.all():
            speaker.order_admin_url = speaker.get_ticket_order_admin_url(self.object.edition)
            extra_context['speakers'].append(speaker)

        # If the submitter of an event is in the list of speakers, force it to
        # be at the beginning of the list. This is used to offer a small degree of
        # control on how the list of speakers is ordered. For more ordering
        # capabilities, we would need to add an order field to the M2M table.
        if self.object.user.profile in extra_context['speakers']:
            extra_context['speakers'].remove(self.object.user.profile)
            extra_context['speakers'].insert(0, self.object.user.profile)

        if not self.request.user.is_anonymous:
            extra_context['is_favorite'] = self.object.favorites.filter(
                pk=self.request.user.pk
            ).exists()
            extra_context['is_going'] = self.object.attendees.filter(
                pk=self.request.user.pk
            ).exists()
        else:
            extra_context['is_favorite'] = None
            extra_context['is_going'] = None

        return super().get_context_data(**{**kwargs, **extra_context})


class PresentationReviewView(
    SingleObjectStoreMixin[Event], LoginRequiredMixin, PermissionRequiredMixin, TemplateView
):
    model = Event
    template_name = 'conference_main/presentations/review.pug'

    def has_permission(self) -> bool:
        return permissions.can_review_event(self.object, self.request.user)

    def get_message_exchange(self):
        message_exchange = list(self.object.messages.all().reverse())

        for idx, m in enumerate(message_exchange):
            if idx == 0:
                m.is_expanded = True
            else:
                if m.message.user != message_exchange[idx - 1].message.user:
                    m.is_expanded = True
                    break

        message_exchange.reverse()

        return message_exchange

    def get_context_data(self, **kwargs):
        extra_context = {
            'permissions': {
                'can_change': permissions.can_change_event(self.object, self.request.user),
                'can_view_messages': permissions.can_view_messages_on_event(
                    self.object, self.request.user
                ),
                'can_add_message': permissions.can_add_message_to_event(
                    self.object, self.request.user
                ),
            },
            'message_form': MessageForm(),
            'message_exchange': [],
            'speakers': [],
        }

        for speaker in self.object.speakers.all():
            speaker.order_admin_url = speaker.get_ticket_order_admin_url(self.object.edition)
            speaker.other_events = speaker.events.exclude(id=self.object.id).all()
            extra_context['speakers'].append(speaker)

        if extra_context['permissions']['can_view_messages']:
            extra_context['message_exchange'] = self.get_message_exchange()

        return super().get_context_data(**{**kwargs, **extra_context})


class PresentationUpdateView(
    LoginRequiredMixin,
    SingleObjectStoreMixin[Event],
    PermissionRequiredMixin,
    SingleObjectTemplateResponseMixin,
    ModelFormMixin,
    ProcessFormView,
):
    model = Event
    template_name = 'conference_main/presentations/update.pug'

    def dispatch(self, request, *args, **kwargs):
        base_fields = ['name', 'description', 'picture', 'slides_url']
        if request.user.has_perm('can_change_event'):
            # Allow staff to edit status of a presentation
            self.fields = base_fields + ['status', 'recording']
        else:
            self.fields = base_fields

        return super().dispatch(request, *args, **kwargs)

    def has_permission(self) -> bool:
        return permissions.can_change_event(self.object, self.request.user)

    def get_success_url(self):
        return urls.reverse(
            'presentation_review',
            kwargs={'edition_path': self.object.edition.path, 'pk': self.object.pk},
        )


class MessagePresentationFormView(
    LoginRequiredMixin, SingleObjectStoreMixin[Event], PermissionRequiredMixin, FormView
):
    """Form view for the submission of a message."""

    model = Event
    form_class = MessageForm

    def has_permission(self) -> bool:
        return permissions.can_add_message_to_event(self.object, self.request.user)

    def form_valid(self, form):
        new_message = Message(content=form.cleaned_data['message'], user=self.request.user)
        new_message.save()
        new_message_exchange = MessageExchange(message=new_message, content_object=self.object)
        new_message_exchange.save()

        return HttpResponseRedirect(
            urls.reverse(
                'presentation_review',
                kwargs={'edition_path': self.object.edition.path, 'pk': self.object.pk},
            )
        )


class PresentationListMixin(PermissionRequiredMixin, View):
    model: Optional[Type[Model]] = Event
    context_object_name: Optional[str] = 'events'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def has_permission(self) -> bool:
        has_perm = permissions.can_view_schedule(self.edition, self.request.user)
        return has_perm

    def get_queryset(self) -> 'QuerySet[Event]':
        """Fetch events, with different sorting depending on display."""
        display = self.request.GET.get('display', 'vertical')
        if display == 'vertical':
            order = ['time', 'day__number']
        else:
            order = ['day__number', 'location__order', 'time']

        status = ['accepted']
        if self.request.user.is_staff:
            status.append('under_review')

        return (
            Event.objects.filter(status__in=status, edition=self.edition)
            .prefetch_related(
                'tags',
                'speakers',
                'location',
            )
            .select_related('day')
            .order_by(*order)
            .all()
        )


class ScheduleView(PresentationListMixin, ListView):
    def get_template_names(self) -> List[str]:
        display = self.request.GET.get('display', 'vertical')
        if display not in {'vertical', 'horizontal', 'sheet_vertical', 'sheet_horizontal'}:
            display = 'vertical'
        return [f'conference_main/presentations/schedule_{display}.pug']

    def _set_events_overlap(self, events: QuerySet[Event]):
        """Given a list of events, set is_overlapping to True if the
        previous event ends after the current one starts.
        """
        for i in range(1, len(events)):
            next_event = events[i]
            prev_event = events[i - 1]
            if next_event.start_time_in_minutes < (
                prev_event.start_time_in_minutes + prev_event.duration_minutes
            ):
                next_event.is_overlapping = True

            if next_event.start_time_in_minutes == prev_event.start_time_in_minutes:
                next_event.is_same_start_time = True

    def get_events_by_time_per_day(self, scheduled_events):
        scheduled_events_per_time_per_day = []
        for time, events_per_time in itertools.groupby(scheduled_events, key=lambda e: e.time):
            events_per_time_per_day = {
                d: list(es) for d, es in itertools.groupby(events_per_time, key=lambda e: e.day)
            }
            scheduled_events_per_time = []
            for day in self.edition.days.order_by('number'):
                scheduled_events_per_time.append((day, events_per_time_per_day.get(day, [])))

            scheduled_events_per_time_per_day.append((time, scheduled_events_per_time))
        return scheduled_events_per_time_per_day

    def get_events_per_day_per_location_with_range(self, scheduled_events):
        scheduled_events_per_day_per_location = {}
        end_of_day_minutes = (23 * 60) + 59  # 1439: closest time to midnight
        schedule_range_minutes = {'start': end_of_day_minutes, 'end': 0}

        # The values of start and end are just defaults, and get updated based on the events
        # added to the schedule (see update schedule range below).

        for e in scheduled_events:
            if not e.duration_minutes:
                continue

            if not e.start_time_in_minutes:
                continue

            # Update schedule range
            st = e.start_time_in_minutes
            if st < schedule_range_minutes['start']:
                schedule_range_minutes['start'] = st
            if st + e.duration_minutes > schedule_range_minutes['end']:
                schedule_range_minutes['end'] = st + e.duration_minutes

            # Ensure day key exists
            if e.day not in scheduled_events_per_day_per_location:
                scheduled_events_per_day_per_location[e.day] = {}

            # Ensure location key exists
            if e.location.slug not in scheduled_events_per_day_per_location[e.day]:
                scheduled_events_per_day_per_location[e.day][e.location.slug] = []

            # Place event in the appropriate track
            scheduled_events_per_day_per_location[e.day][e.location.slug].append(e)

        # Iterate over the sorted event to check for overlaps
        # If an overlap is found, add the 'is_overlapping' property to event, to be used
        # for styling in the schedule.
        for _, locations in scheduled_events_per_day_per_location.items():
            for _, events in locations.items():
                self._set_events_overlap(events)

        # Calculate the width assuming that:
        # - a day of events lasts 7-10 hours
        # - 1min is 10px
        minute_to_pixel_multiplier = 10

        # Build a list of hour slots to display time in the template.
        time_slots = []
        day_start_hour = int(schedule_range_minutes['start'] / 60)
        day_end_hour = int(schedule_range_minutes['end'] / 60)

        for t in range(day_start_hour, day_end_hour + 1):
            time_slots.append(t)

        return {
            'events': scheduled_events_per_day_per_location,
            'range_minutes': schedule_range_minutes,
            'time_slots': time_slots,
            'minute_to_pixel_multiplier': minute_to_pixel_multiplier,
            'width_px': f"{(schedule_range_minutes['end'] - schedule_range_minutes['start']) * minute_to_pixel_multiplier }px",  # noqa: E501, E202
        }

    def get_context_data(self, **kwargs: object) -> Dict[str, object]:
        ctx = super().get_context_data(**kwargs)

        events: Sequence[Event] = ctx['events']

        # If an event does not have a day yet, count it as unscheduled
        ctx['events_unscheduled_count'] = sum(1 for e in events if not e.day or not e.time)

        scheduled_events = [e for e in events if e.day and e.time]

        ctx[
            'events_per_day_per_location_with_range'
        ] = self.get_events_per_day_per_location_with_range(scheduled_events)
        ctx['events_per_time_per_day'] = self.get_events_by_time_per_day(scheduled_events)
        ctx['days'] = self.edition.days.all().order_by('number')
        ctx['edition'] = self.edition

        if not self.request.user.is_anonymous:
            favorites = self.request.user.favorites.all()
            ctx['is_favorite'] = {event: event in favorites for event in scheduled_events}
            attending = self.request.user.attending.all()
            ctx['is_going'] = {event: event in attending for event in scheduled_events}
        else:
            ctx['is_favorite'] = None
            ctx['is_going'] = None

        ctx['filters'] = {
            'location': self.edition.locations.all(),
            'category': sorted(
                set((e.category, e.get_category_display()) for e in scheduled_events)
            ),
        }

        return ctx


class PresentationListJSONView(PresentationListMixin, ListView):
    """Return a JSON list of all presentations in an Event, in no particular order."""

    def render_to_response(
        self, context: Dict[str, object], **response_kwargs: object
    ) -> JsonResponse:
        event_json = {
            event.id: event.as_json() for event in cast(Iterable[Event], context['events'])
        }
        return JsonResponse(event_json)


class SpeakersView(PermissionRequiredMixin, ListView):
    model = Profile

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def has_permission(self) -> bool:
        return permissions.can_view_speakers(self.edition, self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Random sorting of speakers
        speakers = list(context['object_list'])
        random.shuffle(speakers)
        context['speakers'] = speakers
        return context

    def get_queryset(self) -> 'QuerySet[Profile]':
        return Profile.objects.filter(
            events__status='accepted', events__edition=self.edition
        ).distinct()

    def get_template_names(self) -> List[str]:
        return [
            f'conference_main/{self.edition.path}/presentations/speakers.pug',
            'conference_main/presentations/speakers.pug',
        ]


class AttendeesView(PermissionRequiredMixin, ListView):
    model = Profile
    template_name = 'conference_main/presentations/attendees.pug'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def has_permission(self) -> bool:
        return permissions.can_view_attendees(self.edition, self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Random sorting of attendees
        attendees = list(context['object_list'])
        random.shuffle(attendees)
        context['attendees'] = attendees
        return context

    def get_queryset(self) -> 'QuerySet[Profile]':
        return self.edition.get_attendees()
