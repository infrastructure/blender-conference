# Generated by Django 3.2.13 on 2022-09-23 12:57

import conference_main.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0055_album_is_upload_open'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='files',
        ),
        migrations.AddField(
            model_name='event',
            name='slides_url',
            field=models.URLField(blank=True, help_text='A link to a public URL hosting all the files for your presentation. Google Drive, Dropbox, etc.', max_length=255, verbose_name='Slides URL'),
        ),
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(blank=True, help_text='Short (50-100 words) description to help attendees understand what the presentation is about.'),
        ),
        migrations.AlterField(
            model_name='event',
            name='name',
            field=models.CharField(help_text='A short (5-10 words) title for your presentation.', max_length=255),
        ),
        migrations.AlterField(
            model_name='event',
            name='picture',
            field=models.ImageField(blank=True, height_field='picture_height', help_text='A 16:9 picture representing your presentation. Please avoid text.', upload_to=conference_main.models.get_event_picture_upload_path, width_field='picture_width'),
        ),
        migrations.AlterField(
            model_name='event',
            name='proposal',
            field=models.TextField(help_text='Describe the content of your presentation, highlighting its relevance and novelty.'),
        ),
    ]
