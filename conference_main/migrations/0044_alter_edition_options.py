# Generated by Django 3.2.13 on 2022-09-14 14:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0043_update_event'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='edition',
            options={'ordering': ['-year', '-path']},
        ),
    ]
