# Generated by Django 3.2.13 on 2022-11-01 18:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0063_album_constraints'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='recording',
            field=models.CharField(blank=True, help_text='Recording URL that supports oembed.', max_length=255),
        ),
    ]
