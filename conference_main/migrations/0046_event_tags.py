# Generated by Django 3.2.13 on 2022-09-16 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0045_alter_day_options'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('slug', models.SlugField(allow_unicode=True, max_length=100, unique=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='event',
            name='tags',
            field=models.ManyToManyField(blank=True, related_name='events', to='conference_main.Tag'),
        ),
    ]
