from pathlib import Path
from selenium import webdriver

from django.core.management.base import BaseCommand


def ensure_output_directory(location):
    # Get the current script's directory as a Path object
    current_directory = Path(__file__).resolve().parent

    # Combine the current directory and the relative path to create the target path
    target_path = current_directory / 'screens' / location

    # Check if the directory exists, and create it if it doesn't
    if not target_path.is_dir():
        try:
            target_path.mkdir(parents=True, exist_ok=True)
            print(f"Directory '{target_path}' created successfully.")
        except OSError as e:
            print(f"Error creating directory '{target_path}': {e}")
            raise e

    return target_path


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Location names and day numbers to iterate over
        location_names = ['theater', 'classroom', 'market', 'studio', 'sig', 'attic']
        day_numbers = [23, 24, 25]
        edition = 2024

        # Create a headless Chrome browser
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')  # Run in headless mode (no GUI)

        browser = webdriver.Chrome(options=options)

        # Iterate over location names and day numbers
        for location_name in location_names:
            base_path = ensure_output_directory(location_name)
            for day_number in day_numbers:
                url = f'https://conference.blender.org/{edition}/panel/location/{location_name}/?day={day_number}'
                browser.get(url)

                # Set the window size for the screenshot (1080x1920)
                browser.set_window_size(1080, 1920)

                # Capture a screenshot
                file_path = base_path / f'{location_name}_{day_number}.png'
                browser.save_screenshot(file_path)
                print(f"Saved screenshot {file_path}")

        # Close the browser
        browser.quit()
