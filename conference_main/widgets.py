from django import forms
from sorl.thumbnail import get_thumbnail


class ImageWidget(forms.ClearableFileInput):
    template_name = 'conference_main/widgets/image.html'


class PortraitImageWidget(forms.ClearableFileInput):
    template_name = 'conference_main/widgets/image.html'

    def format_value(self, value):
        formatted_value = super().format_value(value)

        if formatted_value:
            return get_thumbnail(formatted_value, '200x200', crop='50%', format='JPEG')
