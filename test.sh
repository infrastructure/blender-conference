#!/usr/bin/env sh
set -eux

env PYTHONPATH=. mypy .
./manage.py test --parallel
black --check .
